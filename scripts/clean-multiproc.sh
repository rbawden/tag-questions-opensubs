# Multi-process for cleaning of OpenSubtitles corpus

# Date: 13/11/16

#======================
# Set shortcuts here

docs=/Users/rbawden/Documents
scripts=$docs/scripts
os_data=$docs/data/parallel/OpenSubtitles2016/en-fr_2000-2016
folder=$os_data/tmp

#======================
# Set variables here

lang=$1
num_things=$2
big_file=$3
name_output_file=$4
#======================
# Default values

if [[ -z "${lang// }" ]]; then
	lang=fr
fi
if [[ -z "${num_things// }" ]]; then
	num_things=4
fi
if [[ -z "${big_file// }" ]]; then
	big_file=$os_data/raw.precleaned.birecoded.2000-2016.$lang
fi
if [[ -z "${name_output_file// }" ]]; then
	name_output_file=raw.precleaned.birecoded.cleaned.2000-2016.$lang
fi
#======================

big_file_basename=$(basename "${big_file}")
mainfolder=$(dirname "${big_file}")


# 1) Split big file into little files
bash $scripts/split-stick-file.sh $big_file $num_things split $folder


echo "Spawning $num_things processes"
echo -e "Processing file list:\nc"
echo "`ls $folder/$big_file_basename.tmp.[a-z][a-z]`"

# 2) Put the script and arguments to be multiprocessed here
for i in `ls $folder/$big_file_basename.tmp.[a-z][a-z]`; do
	little_file_basename=$(basename "${i}")
	#======================
	# Insert script and arguments here
	
    ( python3 $scripts/clean-up-subs.py $i $lang > $folder/$little_file_basename.cleaned & )
	#======================
done

exit

# 3) Stick many files back together again
cat `ls $folder/$big_file_basename.tmp.[a-z][a-z].cleaned` > $folder/$big_file_basename.cleaned.merged
mv $folder/$big_file_basename.cleaned.merged $mainfolder/$name_output_file
