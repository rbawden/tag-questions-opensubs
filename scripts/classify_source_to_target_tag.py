# classify as producing a tag or not
import re, argparse, os, marshal
from math import log
from ast import literal_eval
import gzip

stop_words_fr = ['de', 'le', 'la', 'les', 'leur', 'du', 'des', 'et',
                 'ou', 'ni', 'ne', 'pas', 'je', 'tu', 'il', 'elle',
                 'nous', 'vous', 'ils', 'elles', 'se', "s'", "m'",
                 "t'", "n'", 'y', 'en', "d'", "l'", "j'"]


# tokenised source file (containing tags and non tags)
# def read_source_file(filename):
#     sents = []
#     with open(filename, "r", encoding="utf-8") as fp:
#         for line in fp:
#             sents.append(line.split())
#     return sents



def read_g_file(filename, k=100):
    os.sys.stderr.write("Reading g-test file\n")
    words = []
    with gzip.open(filename, "rt") as fp:
        for i, line in enumerate(fp.readlines()):
            if i==k: break
            line = line.split("\t")[0]
            line = line.strip('"(')
            line = line.strip('\')"')
            line =  tuple(line.split("', '"))
            words.append(line)#.split("\t")[0].strip())
            #print(words)
            #input()
    return words

            
def read_source_file(filename, gram=1):
    sents = []
    os.sys.stderr.write("reading source sentences, gram = "+str(gram)+"...")
    os.sys.stderr.flush()
    with open(filename, "r", encoding="utf-8") as fp:
        for line in fp:
            line = "$BEGIN$ "*(gram-1)+line+" $END$"*(gram-1)
            line = line.split()
            grams = []
            if gram==1: sents.append(line)
            else:
                for i in range(len(line)-gram):
                    grams.append(tuple(line[i:i+gram-1]))
                sents.append(grams)
    os.sys.stderr.write("done\n")
    os.sys.stderr.flush()
    return sents

# read file containing lines that indicate tags
def read_tag_file(line_filename, numlines):
    os.sys.stderr.write("reading tag line file...")
    os.sys.stderr.flush()
    y = [False]*numlines
    with open(line_filename, "r", encoding="utf-8") as fp:
        for line in fp:
            if int(line.strip())-1 > len(y): break
            y[int(line.strip())-1] = True
    os.sys.stderr.write("done\n")
    os.sys.stderr.flush()
    return y


# get vocab
def get_vocab(sents, hapax=-1):
    os.sys.stderr.write("getting vocabulary...")
    os.sys.stderr.flush()
    vocab  = {}
    for sent in sents:
        for word in sent:
            if word not in vocab: vocab[word] = 0
            vocab[word] += 1
    os.sys.stderr.write("done ("+str(len(vocab))+" words in vocabulary)\n")

    # filter very rare ones
    filt_vocab = set([])
    if hapax>0:
        os.sys.stderr.write("filtering words with occs <= "+str(hapax)+"...")
        os.sys.stderr.flush()
        for word in vocab:
            if vocab[word] <= hapax: continue
            else: filt_vocab.add(word)
        os.sys.stderr.write("done ("+str(len(filt_vocab))+" words left)\n")
    else:
        filt_vocab = set(keys(vocab))
    
    return filt_vocab


    
# get non-language specfic features

de_tags = ["nicht wahr", "oder", "ne", "ja", "gell", "nä", "ne"]

fr_tags = ["n\' est \-ce pas", "non", "hein", "pas vrai", "d\' accord",
            "ou quoi", "tu ne crois pas", "tu ne trouves? pas", "c\' est ça",
            "dis", "dites", "ok", "okay", "voulez \-vous", "veux \-tu", "si",
            "oui", "alors", "tu vois", "tu ne vois pas", "vois \-tu",
            "tu crois", "crois \-tu", "souviens \-toi", "souvenez \-vous"]
    
cs_tags = ["ne", "že", "co", "vid'(te)?", "že ne", "že ?jo", "ano", "nemyslíte", "jo",
           "že ano", "nebo n[eé]", "co (tomu)? říká(š|te)", "no ne", "vid' že ne", "nemám pravdu",
           "nebo jo", "že je to tak", "nezdá se vám", "je to tak",
           "nebo snad jo", "není-liž pravda", "co tomu říkáte",
           "není to tak", "no nemám pravdu", "no řekněte", "no nemyslíte", "to je snad jasné",
           "víš o tom", "rozumíš", "nechceš", "snad", "nezdá se vám", "řekni"]

xcs_tags = ["ne", "že", "co", "vid'(te)?", "že ne", "že ?jo", "ano", "nemyslíte", "jo",
           "že ano", "nebo n[eé]", "co (tomu)? říká(š|te)", "no ne", "vid' že ne", "nemám pravdu",
           "nebo jo", "že je to tak", "nezdá se vám", "xxx?co myslí(š|te)", "je to tak",
           "nebo snad jo", "není-liž pravda", "co tomu říkáte",
           "není to tak", "no nemám pravdu", "no řekněte", "no nemyslíte", "to je snad jasné",
           "víš o tom", "xxx?všiml jste si", "xxxnevzpomínáš si",
           "xxxje to tady", "xxxmáš za co", "rozumíš", "nechceš", "xxxjakjinak", "xxxco se dá dělat",
           "xxxvidím ti to na očích", "snad", "nezdá se vám", "xxx?řekni", "xxx!vy ne"]

non_viable_anchor_cs = ["ne", "jo", "prosím", "ano", "oh", "ach", "co", "uh", "ó", "bože", "tak", "vážně",
                        "ale", "och", "no", "velmi dobře", "óó", "sakra", "áh"] # 1 word? starts with capital
    
punct_and_space = "[\.\,\:\;\/\\\+\=\)\(\-\?\!\'\"\` ]"
cs_tag_regex = re.compile("(.*?)\, ("+"|".join(cs_tags)+")( [\.\?\!\"\' ]*)?$", re.I)
cs_notanchor = re.compile("^("+"|".join(non_viable_anchor_cs)+"|"+"|".join(cs_tags)+"|"+punct_and_space+")*$")
fr_tag_regex = re.compile("(.*?)\, ("+"|".join(fr_tags)+") \?", re.I)
de_tag_regex = re.compile("(.*?)\, ("+"|".join(de_tags)+") \?", re.I)


single_uppercase_word = re.compile("^"+punct_and_space+"*[A-ZÉÊÈËÏÎÌÍÔÒÖÓÛÜÙÚÂÄÀÁÝĎĚŘŠŤŮŽČ][^ ]*"+punct_and_space+"*")


def is_viable_anchor(anchor, lang):
    if lang=="cs":
        if re.match(cs_notanchor, anchor) or re.match(single_uppercase_word, anchor):
            return False
        else: return True

def has_tag_in_source(sent, lang):
    # print(sent)
    if lang=="cs":
        tq = re.match(cs_tag_regex, sent)
        if tq:
            anchor = tq.group(1)
            if is_viable_anchor(anchor, lang): return True
    elif lang=="fr":
        tq = re.match(fr_tag_regex, sent)
        if tq: return True

    elif lang=="de":
        tq = re.match(de_tag_regex, sent)
        if tq: return True
    return False


def is_a_question(sent):
    if re.match(".*? \?[\'\"\! ]*", sent): return True
    else: return False

def sort_by_len(list_str):
    list_str.sort(key = lambda x: len(x))

    # print(list_str)
    return list_str

# https://hal.archives-ouvertes.fr/hal-00911083/document
# can echo the verb of the question...
cs_reply = sort_by_len(["ano", "ne", "prosím", "promiňte", "děkuji", "ok", "fajn",
            "dob[řr][ée]", "samozřejmě", "jistě", "rozhodně ne",
            "žádném případě","samozřejmě že ne", "ovšemže ne",
            "rozhodně", "určitě"])

def has_reply(sent, nextsent, cs_reply2):
    for c in cs_reply2:
        if re.match(c+"( |$)", nextsent):
            return c
    return "NA"



def get_features_gram(sent, gwords, gram=1, featnum=0):
    sent = "$BEGIN$ "*(gram-1)+sent+" $END$"*(gram-1)
    #print(sent)
    splitsent = sent.split(" ")
    gramsent = [tuple(splitsent[w:w+gram]) for w in range(len(splitsent)-gram+1)]
    
    
    #x= ["T" if g in gramsent else "False" for i, g in enumerate(gwords)]
    x= [str(i+featnum)+":1"  for i, g in enumerate(gwords) if g in gramsent]
    #for g in gwords:
        #print(g)
        #if g in gramsent:
        #    print(str(g)+"yes")
        #    input()
        #else: print(str(g))
    #input()
    return x

# get language_specific features
def get_features(sentfile, tagfile, lang, uni_g_file, bi_g_file, tri_g_file):
    os.sys.stderr.write("Preparing feature data\n")
    uni_sents = read_source_file(sentfile)
    # bi_sents = read_source_file(sentfile, 2)
    # tri_sents = read_source_file(sentfile, 3)
    tag_lines = read_tag_file(tagfile, len(uni_sents))
    
    # uni_g = marshal.load(open(uni_g_file, "rb"))[0:100] #gtest(uni_sents, tag_lines)
    # bi_g = marshal.load(open(bi_g_file, "rb"))[0:100] #gtest(bi_sents, tag_lines)
    # tri_g = marshal.load(open(tri_g_file, "rb"))[0:100]

    uni_g = read_g_file(uni_g_file, 500)
    bi_g = read_g_file(bi_g_file, 500)
    tri_g = read_g_file(tri_g_file, 500)

    if lang=="cs": list_replies = sort_by_len(cs_reply)

    data = []
    os.sys.stderr.write("Writing features\n")
    for i, (uni_sent, tag) in enumerate(zip(uni_sents, tag_lines)):
        if i%1000==0: os.sys.stderr.write("\r   "+str(i))
        
        if tag: x = ["1"]
        else: x = ["-1"]
        sent = " ".join(uni_sent)
        if i < len(uni_sents)-1: nextsent = " ".join(uni_sents[i+1])
        else: nextsent="NA"

        # basic features
        #x.append("1:"+str(has_tag_in_source(sent, lang))+" ")
        #x.append("2:"+str(is_a_question(sent)))

        if has_tag_in_source(sent, lang):
            x.append("1:1")
        if is_a_question(sent):
            x.append("2:1")


        # g-tests
        # x.append("Uni "+" ".join(["True" if gram in uni_g else "False" for gram in uni_sents[i]]))
        # x.append("Bi "+" ".join(["True" if gram in bi_g else "False" for gram in bi_sents[i]]))
        # x.append("Tri "+" ".join(["True" if gram in tri_g else "False" for gram in tri_sents[i]]))
        #x.append("Uni "+" ".join(get_features_gram(sent, uni_g, 1)))
        #x.append("Bi "+" ".join(get_features_gram(sent, bi_g, 2)))
        #x.append("Tri "+" ".join(get_features_gram(sent, tri_g, 3)))

        x.append(" ".join(get_features_gram(sent, uni_g, 1, 3)))
        x.append(" ".join(get_features_gram(sent, bi_g, 2, 503)))
        x.append(" ".join(get_features_gram(sent, tri_g, 3, 1003)))
        
        # contextual features
        # x.append("Reply "+has_reply(sent, uni_sents[i+1], list_replies))
        
        # data.append(x)
        #os.sys.stderr.write(str(x)+"\n")
        #input()
        #os.sys.stdout.write("|".join(x)+"\n")
        print(" ".join(x))

    # return data



if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("source_file")
    argparser.add_argument("taglinefile")
    argparser.add_argument("-g", nargs=3, help="ngram files from gtest", metavar=('uni', 'bi', 'tri'), required=True)
    argparser.add_argument("-l", choices=["de", "fr", "cs"], required=True)
    args = argparser.parse_args()
    lang = args.l

    # source_sents = read_source_file(args.source_file)
    # tag_lines = read_tag_file(args.taglinefile, len(source_sents))

    uni, bi, tri = tuple(args.g[0:3])

    get_features(args.source_file, args.taglinefile, lang, uni, bi, tri)

    # os.sys.stderr.write("Writing output\n")
    # for d in data:
        # d = [str(x) for x in d]
        # os.sys.stdout.write("|".join(d)+"\n")
    # gtest(source_sents, tag_lines)

    

    # get_langspec_features(source_sents, lang)    
    # vocab = get_vocab(source_sents, 50)

    



