from evaluate_precision_gramtags import *
from sklearn.metrics import precision_recall_fscore_support
import numpy as np
import argparse
argparser = argparse.ArgumentParser()
argparser.add_argument("gold")
argparser.add_argument("pred")
args = argparser.parse_args()



y_true = []
y_pred = []
with open(args.gold) as gf, open(args.pred) as pf:
    for g, p in zip(gf, pf):
        y_true.append(int(g.split()[0].strip()))
        y_pred.append(int(p.split()[0].strip()))

classes=sorted(set(y_true).union(set(y_pred)))

print(classes)
cm = confusion_matrix(y_true, y_pred)
print(cm)

prec, recall, fscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                          np.array(y_pred),
                                                          average='micro')

print("Micro:")
print("\tP:"+str(prec))
print("\tR:"+str(recall))
print("\tF:"+str(fscore))

# binary
binprec, binrecall, binfscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                                   np.array(y_pred),
                                                                   pos_label=1,
                                                                   average='binary')

print("Binary (True):")
print("\tP:"+str(binprec))
print("\tR:"+str(binrecall))
print("\tF:"+str(binfscore))

# for each label
score_per_label = precision_recall_fscore_support(np.array(y_true),
                                                  np.array(y_pred),
                                                  labels=classes,
                                                  average=None)
#print(score_per_label)
print("Per label:")
for i, label in enumerate(classes):
    print("Class "+str(label))
    print("\tP:"+str(score_per_label[0][i]))
    print("\tR:"+str(score_per_label[1][i]))
    print("\tF:"+str(score_per_label[2][i]))




