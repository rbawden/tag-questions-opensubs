import argparse
from newevaluation import *
from readfile import *

def analyse(y_true, y_pred):
    evallist = []
    # loop around all pred examples
    for true, pred in zip(y_true, y_pred):

        # get class for pred
        if pred == "none":
            tag = "none"
        elif re.match(get_en_tags(), pred):
            tag = "gram"
        elif re.match(get_regex_en_special_tags(), pred):
            tag = "misc"
        else:
            # print("*"+pred+"*")
            # input()
            tag = "gram"

        # get class for true
        if true == "none":
            gtag = "none"
        elif re.match(get_en_tags(), true):
            gtag = "gram"
        elif re.match(get_regex_en_special_tags(), true):
            gtag = "misc"
        else:
            gtag = "gram"


        # calculate individual precision
        if tag == gtag:
            if tag == "gram":
                evallist.append(("gram", True))
            elif tag == "misc":
                evallist.append(("lex", True))
            elif tag =="none":
                evallist.append(("none", True))
        else:
            evallist.append((tag+"_"+gtag, False))

    return evallist

def analyse_evallist(evallist, data, y_pred):

    label2idx = {"none": 0, "gram": 1, "lex": 2}
    
    correct_tq_src = [0,0,0]
    correct_tq_tgt = [0,0,0]
    correct_tq_srctgt = [0,0,0]
    correct_notq = [0,0,0]
    for (label, true), row, pred in zip(evallist, data, y_pred):
        
        if true:
            idx = label2idx[label]
            if "hasTaginSrc" in row and "tagTrans" in row:
                correct_tq_srctgt[idx] += 1
            elif "hasTaginSrc" in row:
                correct_tq_src[idx] += 1
            elif "tagTrans" in row:
                correct_tq_tgt[idx] += 1
            else:
                # if label!="none":
                #     print(row)
                #     print(label)
                #     print(pred)
                #     input()
                correct_notq[idx] += 1

    print(correct_tq_src)
    print(correct_tq_tgt)
    print(correct_tq_srctgt)
    print(correct_notq)
        

if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("gold")
    argparser.add_argument("preds")
    argparser.add_argument("data")
    args = argparser.parse_args()

    y_true, y_pred, classes = get_true_pred(args.gold, args.preds)
    y_true, y_pred = normalise_classes(y_true, y_pred)
    data = read_text_file(args.data)

    evallist = analyse(y_true, y_pred)
    analyse_evallist(evallist, data, y_pred)
