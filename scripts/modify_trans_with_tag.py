import argparse, re, os
from classify_tag_questions import get_en_pros, get_en_aux

def pronoun_ors(guess):
    res = False
    for pro in get_en_pros():
        if len(guess)>=len(pro):
            res = False or guess[-len(pro):]==guess
    return res

def aux_ors(guess):
    res = False
    for aux in get_en_aux():
        if len(guess)>=len(aux):
            res = False or guess[0:len(aux)]==guess
    return res

def neg_ors(guess):
    res = False
    for neg in ["n't", "not"]:
        if len(guess)>=len(neg):
            res = False or neg in guess
    return res

    
def modify_translation(transfile, predfile):
    diff=0
    with open(transfile, "r") as tf, open(predfile, "r") as pf:
        for t, p in zip(tf, pf):
            t = t.strip()
            p = p.strip()

            # if t in ['False', 'none']:
            #     new = t
            # else:
            #     new = t


            # used for all expes up to now
            if t in ['False', 'none']:
                new = p
            if pronoun_ors(t) and p not in ['False', 'none']:
                new = p
            elif neg_ors(t) and p not in ['False', 'none']:
                new = p
            # elif pronoun_ors(p) and t in ['False', 'none']:
                # new = p
            # elif t != p and p not in ['False', 'none']:
                # new = t
            else:
                new=t


            if t in ['False', 'none'] and p not in ['False', 'none']:
                diff+=1
                
            # new = ""
            # # p = p.replace("_", " ").replace("-", "'")
            # if t=="none":
            #     # use the classifier's guess
            #     new = p
            # elif p=="none":
            #     new = t
            # elif p!="none" and t in ["right", "okay", "is_it"]:
            #     new = p
            # new = t
            
            print(new)

    os.sys.stderr.write(str(diff)+"\n")


if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("trans_file")
    argparser.add_argument("predictions")

    args = argparser.parse_args()
    
    modify_translation(args.trans_file, args.predictions)

    
