import argparse, os, gzip


def merge(baseline, classifier1, classifier2):

    # transfile
    if not os.path.exists(baseline) and ".gz" not in baseline: baseline+=".gz" # try .gz file
    elif not os.path.exists(baseline) and ".gz" in baseline: baseline = baseline.replace(".gz", "")
    if ".gz" in baseline: bf = gzip.open(baseline, "rt")
    else:  bf = open(baseline, "r")

    # predfile
    if not os.path.exists(classifier1) and ".gz" not in classifier1: classifier1+=".gz" # try .gz file
    elif not os.path.exists(classifier1) and ".gz" in classifier1: classifier1 = classifier1.replace(".gz", "")
    if ".gz" in classifier1: pf = gzip.open(classifier1, "rt")
    else:  pf = open(classifier1, "r")

    # predfile 2
    if not os.path.exists(classifier2) and ".gz" not in classifier2: classifier2+=".gz" # try .gz file
    elif not os.path.exists(classifier2) and ".gz" in classifier2: classifier2 = classifier2.replace(".gz", "")
    if ".gz" in classifier2: pf2 = gzip.open(classifier2, "rt")
    else:  pf2 = open(classifier2, "r")
        
    for b, p, p2 in zip(bf, pf, pf2):
        
        # baseline more varied (recall better)
        # p better at nones
        # p2 more accurate

        b, p, p2 = b.strip(), p.strip(), p2.strip()

        if p2=="none":
            final=p2
        # elif p=="none" and (b=="none" or p2=="none"):
        #      final="none"
        # elif p2!="none":
        #     final=p2
        elif b!="none" and p!="none":
            final=p
        else:
            final=b

        
        # if b=="none":
        #     final=b
        # else:
        #     final=p

        os.sys.stdout.write(final+"\n")


if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("baseline")
    argparser.add_argument("preds")
    argparser.add_argument("preds2")
    args = argparser.parse_args()
    
    merge(args.baseline, args.preds, args.preds2)


