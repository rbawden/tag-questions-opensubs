import os, argparse

parser = argparse.ArgumentParser()
parser.add_argument("threshold", type=float)
parser.add_argument("predfile")
args = parser.parse_args()

threshold=args.threshold

with open(args.predfile, "r") as pp:
    for i,p in enumerate(pp):
        p = p.strip()
        
        if float(p) > threshold:
            os.sys.stdout.write("True\n")
        else:
            os.sys.stdout.write("False\n")


