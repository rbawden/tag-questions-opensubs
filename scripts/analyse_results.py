from evaluate_precision_gramtags import *
from sklearn.metrics import precision_recall_fscore_support
import numpy as np
import argparse, gzip, os
import pandas as pd

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def get_true_pred(gold, pred):
    y_true = []
    y_pred = []
    if ".gz" in gold: gf = gzip.open(gold, "rt")
    else: gf = open(gold, "r")
    if ".gz" in pred: pf = gzip.open(pred, "rt")
    else: pf = open(pred, "r")

    for g, p in zip(gf, pf):
        # print(g)
        # print(p)
        g, p = g.strip().lower(), p.strip().lower()
        # if g=="" or p=="": break
        y_true.append(g)
        y_pred.append(p)
    if len(y_true)!=len(y_pred):
        exit("Gold and pred do not have an equal number of elements")
    classes = sorted(set(y_true).union(set(y_pred)))
    gf.close()
    pf.close()
    return y_true, y_pred, classes

def get_cross_tab(y_true, y_pred):
    print(pd.crosstab(pd.Series(y_true), pd.Series(y_pred), rownames=['True'], colnames=['Predicted'], margins=True))

def get_f_without_none(y_true, y_pred, show=False):
                
    y_true, y_pred = get_data_no_nones(y_true, y_pred)
    prec, recall, fscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                              np.array(y_pred),
                                                              average='micro')
    if show:
        print("Micro (only on real TQs):")
        print("\tP:"+str(prec))
        print("\tR:"+str(recall))
        print("\tF:"+str(fscore))
    
    return fscore

def analyse_confusion_matrix(y_true, y_pred, classes):


    if len(y_true)!=len(y_pred):
        exit("The predicted and true labels are not the same length")
        
    # classes=sorted(set(y_true).union(set(y_pred)))
    cm = confusion_matrix(y_true, y_pred, labels=classes)
    # fig = plt.figure()
    # plot_confusion_matrix(cm, classes=classes, title='Confusion matrix')

    # fig.set_size_inches(100, 100, forward=True)
    # fig.savefig('/Users/rbawden/Documents/work/tag-questions-opensubs/classify/cs2000-en2000/eval2/plot.real.all.de-en.png', dpi=200)
    # column labels = predicted
    # row labels = true


    # print out confusion matrix

    ct = pd.crosstab(pd.Series(y_true), pd.Series(y_pred) )
    out = open("/Users/rbawden/Documents/work/tag-questions-opensubs/classify/de-en/eval2/plot.real.all.cs2000-en2000.txt", "w")
    out.write(ct.to_string())
    out.close()
    
    # calculate some scores
    true2correct = {}
    true2incorrect = {}
    pred2correct = {}
    pred2incorrect = {}

    
    total = len(y_true)
    numtruenones = len([y for i, y in enumerate(y_true) if y=="none"])
    numprednones = len([y for i, y in enumerate(y_pred) if y=="none"])
    numcorrectnones = len([y for i, y in enumerate(y_true) if y_pred[i]=="none" and y_true[i]=="none"])
    print("total number of utterances = "+str(total))
    print("total number of true nones = "+str(numtruenones))
    print("total number of pred nones = "+str(numprednones))
    print("number of nones correctly predicted = "+str(numcorrectnones)+" (p:"+str(round(numcorrectnones/float(numprednones), 4))+", r:"+str(round(numcorrectnones/float(numtruenones),4))+")")

    correct = len([y for i, y in enumerate(y_pred) if y==y_true[i]])
    print("total number correctly predicted = "+str(correct)+" (p:"+str(round(correct/float(total), 4))+")")
    correct_withoutnones = len([y for i, y in enumerate(y_pred) if y==y_true[i] and y!="none"])
    totalpred_nonones = len([y for i, y in enumerate(y_pred) if y==y_pred[i]!="none"])
    totaltrue_nonones = len([y for i, y in enumerate(y_true) if y==y_true[i]!="none"])
    print("total number correctly predicted (excluding nones correctly predicted) = "+str(correct_withoutnones)+" (p:"+str(round(correct_withoutnones/float(totalpred_nonones), 4))+", r:"+str(round(correct_withoutnones/float(totaltrue_nonones), 4))+")")
    
    input()
    # raw scores - recall
    for i, row in enumerate(cm):
        cl = (i, classes[i])
        total = sum(row)
        correct = row[i]
        incorrect = total-correct
        true2correct[cl] = row[i]
        true2incorrect[cl] = total - row[i]

    # raw scores - precision
    for i, c in enumerate(classes):
        cl = (i, c)
        total=sum([cm[j, i] for j in range(len(classes))])
        correct = cm[i, i]
        incorrect = total - correct
        pred2correct[cl] = int(correct)
        pred2incorrect[cl] = int(incorrect)

    # which are best and worst ?
    print("Best predicted = ")
    blah2correct = true2correct
    blah2incorrect = true2incorrect
    for i, cl in sorted(blah2correct, key=blah2correct.get, reverse=True):
        # colsum = float(sum(cm[:,i]))
        colsum = float(sum(cm[i,:])) # row sum
        if colsum==0: continue
        print(cl+":"+str(blah2correct[(i,cl)])+" ("+str(round(blah2correct[(i, cl)]/colsum,4))+") correct, "+str(blah2incorrect[(i,cl)])+" ("+str(round(blah2incorrect[(i,cl)]/colsum,4))+") incorrect")

    cmn = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis] # normalised on rows

    for i, row in enumerate(cmn):
        print("True class = "+classes[i])
        for j, c in enumerate(row):
            if c!=0:
                print("\tPred class "+classes[j]+":"+str(c))


if __name__=="__main__":

    argparser = argparse.ArgumentParser()
    argparser.add_argument("gold")
    argparser.add_argument("pred")
    # argparser.add_argument("-binary", action='store_true', default=False)
    # argparser.add_argument("-noshow", action='store_true', default=False)
    # argparser.add_argument("-t", choices=["tagnottag", "fine", "finer", "real"], required=True)
    args = argparser.parse_args()

    y_true, y_pred, classes = get_true_pred(args.gold, args.pred)
    
    analyse_confusion_matrix(y_true, y_pred, classes)
    
    # f_micro = get_f_micro(y_true, y_pred, not args.noshow)
    # f_nonone = get_f_without_none(y_true, y_pred, not args.noshow)
    # f_each = get_f_each_label(y_true, y_pred, classes, not args.noshow)

    # os.sys.stdout.write(str(f_nonone)+" "+str(f_micro)+"\n")


