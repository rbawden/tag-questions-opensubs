import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("dataset", choices=["dev1", "dev2", "dev1DEV", "dev2DEV", "dev1TEST", "dev2TEST"])
parser.add_argument("lang", choices=["cs", "fr", "de"])
parser.add_argument("threshold", type=float)
parser.add_argument("predfile")
parser.add_argument("-binary", default=False,action='store_true')
args = parser.parse_args()

dset = args.dataset
DEV=""
if "dev1DEV" in dset:
    dset="dev1"
    DEV = "DEV"
if "dev2TEST" in dset:
    dset="dev2"
    TEST="TEST"
lang=args.lang

    
datasetfolder = "/Users/rbawden/Documents/data/parallel/OpenSubtitles2016/"+lang+"-en_2000-2016/datasets"

if args.lang=="fr":
    sourcefile = datasetfolder+"/"+dset+".raw.precleaned.birecoded.cleaned.noblank.melttok.truecased.2000-2016."+lang
else:
    sourcefile = datasetfolder+"/"+dset+".raw.nocr.cleaned.noblank.melttok.truecased.2000-2016."+lang


tagfolder = "/Users/rbawden/Documents/work/tag-questions-opensubs/classify/"+lang+"-en"
tgtfile = datasetfolder+"/"+dset+".raw.precleaned.birecoded.cleaned.noblank.melttok.truecased.2000-2016.en"
# pred = tagfolder+"/pred/"+dset+DEV+".binary.mainFeatures."+lang+"-en"
gold = tagfolder+"/gold/"+dset+DEV+".gold.tag_nottag."+lang+"-en"

if args.lang=="fr":
    tagfolder="/Users/rbawden/Documents/work/tag-questions-opensubs/translated/fr-en"
    gold = tagfolder+"/gold/"+dset+DEV+".gold.tagnottag."+lang+"-en"


pred=args.predfile
threshold = args.threshold # -0 #-0.9942 # -2 for dev1TEST (to get all)
rangetag = [None,0,0,0]
rangenottag = [None,0,0,0]
correct = 0
notcorrect = 0
tagsfound = 0

# with open(sourcefile, "r") as fp, open(tgtfile, "r") as tp, \
    # open(pred, "r") as pp, open(gold, "r") as gp:
    # for i, (s, t, p, g) in enumerate(zip(fp, tp, pp, gp)):
with open(pred, "r") as pp, open(gold, "r") as gp:
    for i, (p, g) in enumerate(zip(pp, gp)):
        if args.dataset=="dev1DEV" and i > 2043618:
            break
        
        # s, t, p, g = s.strip(), t.strip(), p.strip(), g.strip()
        p, g = p.strip(), g.strip()
        
        if args.binary:
            if float(p) > threshold:
                os.sys.stdout.write("True\n")
            else:
                os.sys.stdout.write("False\n")
        else:
            if float(p)> threshold:
                os.sys.stderr.write(str(i+1)+"\n")
                os.sys.stdout.write("firstclassifier:"+str(round(float(p), 2))+" ")
                if float(p) == 1 :
                    os.sys.stdout.write("isOne ")
                elif float(p) > 0.5:
                    os.sys.stdout.write("greaterThanHalf ")
                elif float(p) > 0:
                    os.sys.stdout.write("greaterThan0 ")
                elif float(p) > -0.5:
                    os.sys.stdout.write("greaterThanMinusHalf ")
                elif float(p) < -0.8:
                    os.sys.stdout.write("LessThanMinusZeroEight ")
                elif float(p) < -0.9:
                    os.sys.stdout.write("LessThanMinusZeroNine ")
                elif float(p) < -0.95:
                    os.sys.stdout.write("LessThanMinusZeroNineFive ")
                elif float(p) < -0.97:
                    os.sys.stdout.write("LessThanMinusZeroNineSeven ")
                elif float(p) < -0.98:
                    os.sys.stdout.write("LessThanMinusZeroNineEight ")
                else:
                    os.sys.stdout.write("VeryLowValue ")

                os.sys.stdout.write("\n")

        # look at ranges to set threshold
        if float(p) < threshold and g=="False":
            correct += 1
        elif float(p) >= threshold and g=="True":
            correct += 1
            tagsfound += 1
        else:
            notcorrect += 1
                

        if g=="True":
            rangetag[2] += 1
            rangetag[3] += float(p)
            if rangetag[1] < float(p): rangetag[1] = float(p)
            if not rangetag[0]: rangetag[0] = float(p)
            elif rangetag[0] > float(p): rangetag[0] = float(p)

        elif g=="False":
            rangenottag[2] += 1
            rangenottag[3] += float(p)
            if rangenottag[1] < float(p): rangenottag[1] = float(p)
            if not rangenottag[0]: rangenottag[0] = float(p)
            elif rangenottag[0] > float(p): rangenottag[0] = float(p)

        else:
            exit("Problem in gold file\n")
        
# summary
# print("Range for tags = "+str(rangetag[0])+" - "+str(rangetag[1])+" (ave = "+str(round(rangetag[3]/float(rangetag[2]),2))+")")
# print("Range for non-tags = "+str(rangenottag[0])+" - "+str(rangenottag[1])+" (ave = "+str(round(rangenottag[3]/float(rangenottag[2]),2))+")")

# print("With a threshold of "+str(threshold)+", correct = "+str(correct)+", not correct = "+str(notcorrect)+" ("+str(round(correct/float(correct+notcorrect),2))+")")
# print("Out of "+str(rangetag[2])+" tags, number found = "+str(tagsfound)+" ("+str(round(tagsfound/float(rangetag[2]),2))+")")





