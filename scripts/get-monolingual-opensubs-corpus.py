# get monolingual opensubs corpus

import gzip, os, argparse, re

def read_raw_file(file_pointer):

    sent, sentences, meta, startedmeta, startedsubmeta = None, [], {}, False, False
    sid, i = None,0
    for line in file_pointer:
        # print(line)
        # input()
        line = line.decode(encoding="utf-8")
        sentence_match = re.match(r'.*?<s.*? id="([\d]+)".*?>.*?', line)
        if sentence_match:
            sid = int(sentence_match.group(1))
            i+=1
            if sent!=None:
                sent=""
            if sid!=len(sentences)+1:
                print("sentence id = "+str(sid))
                print("number of sentences = "+str(len(sentences)))
                print("There has been an awful error in the sentence id numbers!!!")
                input()
            
        tag_match = re.match(r'.*?<.*?>.*?', line)
        # subtitle text appears on new line (but can be empty)
        # so also add a blank line in this case.
        if not tag_match:
            sent = line.strip()
            sentences.append(sent)
        elif line.strip=="</s>" and sid!=len(sentences):
            sentences.append("")

        # handle meta information
        if "<meta>" in line: startedmeta=True
        elif "</meta>" in line: startedmeta=False
        elif "<subtitle>" in line: startedsubmeta=True
        elif "</subtitle>" in line: startedsubmeta=False
            
        if startedmeta:
            metamatch = re.match(r'[^<>]*<([^<>]+)>([^<>]+)</\1>', line)
            if metamatch:
                if startedsubmeta and metamatch.group(1)=='duration':
                    content = metamatch.group(2).replace("\t", ", ")
                    meta['subduration'] = content
                else:
                    content = metamatch.group(2).replace("\t", ", ")
                    meta[metamatch.group(1)] = content

    return sentences, meta



def generate(folder, lang):
    filmnum = 0
    sentnum = 0
    
    if folder[-1]=="/": folder=folder[:-1]
    for year in os.listdir(folder+"/"+lang):
        if year[0] == ".": continue
        for imdb in os.listdir(folder+"/"+lang+"/"+year):
            if imdb[0] == ".": continue
            for fic in os.listdir(folder+"/"+lang+"/"+year+"/"+imdb):
                if fic[0] == ".": continue
                
                filmnum += 1

                with gzip.open(folder+"/"+lang+"/"+year+"/"+imdb+"/"+fic, "r") as fp:
                    sentences, meta = read_raw_file(fp)

                # deal with meta-info and print out to stderr
                meta["imdb"] = imdb

                encoding, original, confidence, genre, country, subduration, unk = "","","","","","",""
                if 'encoding' in meta: encoding=meta['encoding']
                if 'original' in meta: original=meta['original']
                if 'confidence' in meta: confidence=meta['confidence']
                if 'genre' in meta: genre=meta['genre']
                if 'country' in meta: country=meta['country']
                if 'subduration' in meta: subduration=meta['subduration']
                if 'unknown_words' in meta: unk=meta['unknown_words']
                numsents = len(sentences)
        
                os.sys.stderr.write("\t".join([str(filmnum+1), str(sentnum+1), str(sentnum+numsents),
                                                imdb, year, encoding, original, genre, country,
                                                subduration, unk,
                                                confidence, fic])+"\n")
                sentnum += numsents
                
                # print out lines to stdout
                for i, line in enumerate(sentences):
                    os.sys.stdout.write(line+"\n")


# ---------------------------------------------------------------------------
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Create a parallel corpus from all folders')
    parser.add_argument("-r", "--raw_folder", required=True)
    parser.add_argument("-l", "--lang", required=True)
    args = parser.parse_args()
    generate(args.raw_folder, args.lang)
