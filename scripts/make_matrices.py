
import sys, os

results = sys.argv[1]

lastrow = 1.2
test2train2scores = {}
with open(results, "r") as fp:
    for line in fp:
        row = line.strip().split()
        test = float(row[0])
        train = float(row[1])
        if len(row)!=9 or float(row[2])==train-1:
            1
            # os.sys.stderr.write("malformed line (outputting zeroes): "+str(line))
            # exit()
        if test not in test2train2scores: test2train2scores[test] = {}
        if train not in test2train2scores[test]: test2train2scores[test][train] = tuple(row[2:])
        else:
            os.sys.stderr.write("duplication of test and train hyperparams: "+str(line))
            exit()

# print out

scoring = ["Precision on TQs", "Recall on TQs", "F score on TQs", "Precision on nones", "Recall on nones",
 "F score on nones", "Overall Precision"]
for k, s in enumerate(scoring):
    print(s)
    print("\t"+"\t".join([str(i/10.) for i in range(-11, 11)]))
    for i in range(-11, 11):
        if i/10. not in test2train2scores: break
        os.sys.stdout.write(str(i/10.))
        for j in range(-11, 11):
            if j/10. not in test2train2scores[i/10.]: os.sys.stdout.write("\t-")
            elif len(test2train2scores[i/10.][j/10.])<(k+1): os.sys.stdout.write("\t-")
            else: os.sys.stdout.write("\t"+str(test2train2scores[i/10.][j/10.][k]))
        print("")
    print("\n\n")
        
    
        
        




