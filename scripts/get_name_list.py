import urllib.request
import re, string

fem_url = "https://nameberry.com/search/girls_names/"
male_url = "https://nameberry.com/search/boys_names/"

out_m = "/Users/rbawden/Documents/work/tag-questions-opensubs/resources/guys.list"
out_f = "/Users/rbawden/Documents/work/tag-questions-opensubs/resources/gals.list"

guys = []
gals = []

for letter in string.ascii_lowercase:

    fp = urllib.request.urlopen(male_url+letter.upper())
    contents = fp.read().decode("utf8")
    fp.close()

    names = re.findall('(?<=href="/babyname/)[^<>]+(?=">)', contents)
    guys.extend(names)
    
for letter in string.ascii_lowercase:

    fp = urllib.request.urlopen(fem_url+letter.upper())
    contents = fp.read().decode("utf8")
    fp.close()

    names = re.findall('(?<=href="/babyname/)[^<>]+(?=">)', contents)
    gals.extend(names)

with open(out_m, "w") as fp:
    for g in guys:
        fp.write(g+"\n")

with open(out_f, "w") as fp:
    for g in gals:
        fp.write(g+"\n")
    


