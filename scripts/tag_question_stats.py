# statistics about which auxiliaries/pronouns
# coding: utf-8

import re, operator, os

def get_en_pros():
    return ["I", "you", "[h\']e", "thee", "ye", "yer", "she", "we", "one", "they", "it", "i\'", "there", "someone", "anyone"]

def get_en_aux():
    return ["have", "had", "has", "might", "may", "will", "would", "could",
              "can", "must", "should", "shall", "ought", "do",
              "am", "are", "is", "was", "were", "had", "ai", "dare", "need"]

def get_fr_tag_phrases():
    fr_tag_phrases = ["n\' est \-ce pas", "non", "hein", "pas vrai", "d\' accord",
                      "ou quoi", "tu ne crois pas", "tu ne trouves? pas", "c\' est ça",
                      "dis", "dites", "ok", "okay", "voulez \-vous", "veux \-tu", "si",
                      "oui", "alors", "tu vois", "tu ne vois pas", "vois \-tu",
                      "tu crois", "crois \-tu", "souviens \-toi", "souvenez \-vous"]
    return fr_tag_phrases

def get_en_not_permitted():
    en_not_permitted = ["^.*?(is you|do it|have one|are he|are she|is not you|have it|is not you|not you) *[\.\? \'\"]*$", "^.*?\.\.+[ \?\.\"\']*$"]
    return en_not_permitted

def get_regex_en_special_tags():
    # return re.compile(".+ (inni.|\, right \?|eh \?|\, see \?|\, remember \?|\, you know \?|\, or what \?)[ \"\'\?\.]*$", re.I)
    return re.compile("^(.+) ((?:inn+i.|\, ri+ght \?|e+h+ \?|\, see+ \?|\, remember \?|\, you know \?|\, or what \?|\, ye+a+h+|\, aye|\, you see|\, like|\, ok(ay)?|do n't y..? think)[ \"\'\?\.]*)$", re.I)

def get_en_neg_words():
    return ["n[\'o]t", "never", "no longer", "nobody", "nowhere", "nothin[^ ]?", "no-?one",
            "none", "[h\']ardly", "rarely", "scarcely", "seldom", "barely", "neither",
            "nor", "ne\'er", "[^ ]*nae", "nuffin[^ ]?", "can ?nae", "nowt"] # removed 'no' - treat separately


def tag_questions_stats(en_file):

    stats = {}
    statsmisc = {}
    
    # Regular expressions
    anchor_tag = en_tags = re.compile("(^|.*\, )(("+"|".join(get_en_aux())+") (?:n.t )?"+"("+"|".join(get_en_pros())+")(?: not)?[ \?\.\"\']+?)$", re.I)
    anchor_special_tag = get_regex_en_special_tags()
    nb_no_anchor = 0
    
    # Go through and match sentences
    for i, en in enumerate(en_file):

        # if i==10000: break
        en = en.lower()
        if en.strip()=="": continue
            
        anchor_tag_match = re.match(anchor_tag, en)
        anchor_special_tag_match = re.match(anchor_special_tag, en)

        # Check to see if there is a tag first
        if (anchor_tag_match==None or anchor_tag_match.group(2).strip=="") and \
            (anchor_special_tag_match==None or anchor_special_tag_match.group(2).strip==""):
            continue

        # Get anchor
        if anchor_tag_match: anchor = anchor_tag_match.group(1)
        elif anchor_special_tag_match: anchor = anchor_special_tag_match.group(1)
        else: continue

        # Strip any extra tag questions
        anchor = re.sub("(\, )?("+"|".join(get_en_aux())+") (n.t )?"+"("+"|".join(get_en_pros())+")( not)?[ \?\.\,\"\']*$", "", anchor, re.I)
            
        # Get auxiliary verb
        if anchor_tag_match:
            tag = anchor_tag_match.group(2)
            tag_pro = anchor_tag_match.group(4)
            aux_verb = anchor_tag_match.group(3)
        elif anchor_special_tag_match:
            tag = anchor_special_tag_match.group(2)
            tag_pro = None
            aux_verb = None 
                
        # No anchor 
        if anchor.strip()=="" or len((re.sub("[\.\,]", "", anchor)).split())<2:
            nb_no_anchor += 1
            continue


        # do stats
        if aux_verb:
            if aux_verb not in stats: stats[aux_verb] = {}
            if tag_pro not in stats[aux_verb]:
                stats[aux_verb][tag_pro] = {}
                stats[aux_verb][tag_pro]["neg"] = 0
                stats[aux_verb][tag_pro]["pos"] = 0
            if "not" in tag or "n't" in tag:
                stats[aux_verb][tag_pro]["neg"] += 1
            else:
                stats[aux_verb][tag_pro]["pos"] += 1
                
        else:
            tagword = re.sub("[\,\!\?\.]*", "", tag).strip()
            if tagword not in statsmisc: statsmisc[tagword] = 0
            statsmisc[tagword] += 1
        
    # print out stats
    auxs = {}
    total=0
    for aux in stats:
        numaux = sum([stats[aux][pro]["neg"]+stats[aux][pro]["pos"] for pro in stats[aux]])
        auxs[aux] = numaux
        total += numaux

    for aux, numaux in sorted(auxs.items(), key=operator.itemgetter(1), reverse=True):
        print(aux+" : "+str(numaux)+" ("+str(round(100*(numaux/float(total)), 2))+"%) (pos="+str(sum([stats[aux][pro]["pos"]for pro in stats[aux]]))+", neg="+str(sum([stats[aux][pro]["neg"] for pro in stats[aux]]))+")")
        pros = {}
        for pro in stats[aux]:
            pros[pro] = stats[aux][pro]["neg"]+stats[aux][pro]["pos"]
            
        for pro, num in sorted(pros.items(), key=operator.itemgetter(1), reverse=True):
            neg = stats[aux][pro]["neg"]
            pos = stats[aux][pro]["pos"]
            print("\t"+pro+" : "+str(neg+pos)+" (pos="+str(pos)+", neg="+str(neg)+")")


    # group by lemma
    lemmas = {}
    for aux, numaux in sorted(auxs.items(), key=operator.itemgetter(1), reverse=True):
        if aux in ["is", "are", "am", "were", "was"]:
            lemma = "be"
        elif aux in ["have", "has", "had"]:
            lemma = "have"
        elif aux in ["can", "could"]:
            lemma = "can"
        elif aux in ["will", "would"]:
            lemma = "will"
        elif aux in ["shall", "should"]:
            lemma = "shall"
        elif aux in ["may", "might"]:
            lemma = "may"
        elif aux in ["need"]:
            lemma = "need"
        else:
            os.sys.stderr.write("lemma not yet identified: for "+aux+"\n")
        if lemma not in lemmas: lemmas[lemma] = {}
        for pro in stats[aux]:
            if pro not in lemmas[lemma]:
                lemmas[lemma][pro] = {}
                lemmas[lemma][pro]["neg"] = 0
                lemmas[lemma][pro]["pos"] = 0
            lemmas[lemma][pro]["neg"] += stats[aux][pro]["neg"]
            lemmas[lemma][pro]["pos"] += stats[aux][pro]["pos"]

    print("**********************")
            
    for lemma, _ in sorted(lemmas.items(), key=operator.itemgetter(1), reverse=True):
        pos = sum([lemmas[lemma][pro]["pos"] for pro in lemmas[lemma][pro]])
        neg = sum([lemmas[lemma][pro]["neg"] for pro in lemmas[lemma][pro]])
        numlemma = neg+pos
        print(lemma+" : "+str(numlemma)+" ("+str(round(100*numlemma/float(total), 2))+"%) (pos="+str(pos)+", neg="+str(neg)+")")
        pros = {}
        for pro in lemmas[lemma]:
            pros[pro] = lemmas[lemma][pro]["neg"]+lemmas[lemma][pro]["pos"]
            
        for pro, num in sorted(pros.items(), key=operator.itemgetter(1), reverse=True):
            neg = lemmas[lemma][pro]["neg"]
            pos = lemmas[lemma][pro]["pos"]
            print("\t"+pro+" : "+str(neg+pos)+" (pos="+str(pos)+", neg="+str(neg)+")")

if __name__ == "__main__":

    import argparse
    argparser = argparse.ArgumentParser()
    argparser.add_argument('en_file')
    args = argparser.parse_args()

    with open(args.en_file) as en_file:
        tag_questions_stats(en_file)

                               
