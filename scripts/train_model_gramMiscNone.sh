#!/bin/sh


lambda=$1
set=$2 # tuning data
langpair=$3
scriptdir=$4
vw=$5
outputdir=$6
expdir=$7
reuse=$8


if [ "$reuse" == "True" ] && [ -f "$outputdir/$lambda.trainsmall.gramMiscNone.$langpair" ]; then
	echo "Reloading gram-misc-none model from $outputdir/$lambda.trainsmall.gramMiscNone.$langpair"
else
	echo "Training gram-misc-none model $expdir $outputdir"
	echo "python3 $scriptdir/calculate_weighting.py $expdir/model-seq/classcounts.gramMiscNone.trainsmall.$langpair $lambda)"
	
	# Train $expdir/model-seq on dev $expdir/data (weighted according to $lambda)
	zcat $expdir/data/trainsmall.gramMiscNone.$langpair.gz | perl -pe "s/'/\-/g" |   \
		eval "$(python3 $scriptdir/calculate_weighting.py $expdir/model-seq/classcounts.gramMiscNone.trainsmall.$langpair $lambda)" | \
		perl -pe 's/\t/ |f /' |\
		$vw -f $outputdir/$lambda.trainsmall.gramMiscNone.$langpair  \
			--named_labels `cat $expdir/model-seq/named_labels.gramMiscNone.trainsmall.$langpair` \
			--oaa `cat $expdir/model-seq/num_labels.gramMiscNone.trainsmall.$langpair` -q ff --l2=1e-6 --ftrl
fi
	
# $expdir/predict on trainsmall set
zcat $expdir/data/trainsmall.gramMiscNone.$langpair.gz | perl -pe 's/\s/ |f /' |\
	$vw -i $outputdir/$lambda.trainsmall.gramMiscNone.$langpair  \
		-t -p $outputdir/pred.$lambda.trainsmall.gramMiscNone.$langpair

# $expdir/predict on $set set
perl -pe 's/^/ |f /' $outputdir/tuningtmp.$set |\
	$vw -i $outputdir/$lambda.trainsmall.gramMiscNone.$langpair \
		-t -p $outputdir/pred.$lambda.$set.gramMiscNone.$langpair

# Extract lines that are $expdir/predicted as lexical tags from trainsmall
cat $outputdir/pred.$lambda.trainsmall.gramMiscNone.$langpair |\
	grep "^misc" -n | cut -d":" -f1 > $outputdir/lexlines.trainsmall
python3 $scriptdir/get-these-lines-from-numbers.py $expdir/data/trainsmall.gramMiscTypeNone.$langpair.gz \
			$outputdir/lexlines.trainsmall > $outputdir/lexexamples.fortraining.trainsmall 

# Extract lines that are $expdir/predicted as lexical tags from $set
cat $outputdir/pred.$lambda.$set.gramMiscNone.$langpair \
	| grep -n "^misc" - | cut -d":" -f1 > $outputdir/lexlines.$set 
python3 $scriptdir/get-these-lines-from-numbers.py $outputdir/tuningtmp.$set $outputdir/lexlines.$set \
			> $outputdir/lexexamples.$set

# Get named labels and num labels		
cat $outputdir/lexexamples.fortraining.trainsmall | cut -d" " -f 1 | sort | uniq | perl -pe 's/\n/,/g' |\
	perl -pe 's/,$//' > $outputdir/named_labels.lex.trainsmall.var.$langpair
cat $outputdir/named_labels.lex.trainsmall.var.$langpair | perl -pe 's/,/\n/g' | sed -n '=' |\
	wc -l > $outputdir/num_labels.lex.trainsmall.var.$langpair

cat $outputdir/lexexamples.fortraining.trainsmall  | cut -d" " -f1 |\
	sort | uniq -c > $outputdir/classcounts.lex.trainsmall.var.$langpair  ; \


