
from readfile import read_list
from newevaluation import normalise_classes
import argparse
from scipy.stats import chisquare



def mcnemars(gold, baseline, pred):
    # results
    r = {'bpos': {'ppos': 0, 'pneg': 0}, 'bneg': {'ppos': 0, 'pneg': 0}}
    true_false_b = []
    true_false_p = []
    
    assert(len(gold)==len(baseline)==len(pred))
    
    for g, b, p in zip(gold, baseline, pred):
        if b == g and p == g:
            r['bpos']['ppos'] += 1
        elif b == g:
            r['bpos']['pneg'] += 1
        elif p == g and b != g:
            r['bneg']['ppos'] += 1
        else:
            r['bneg']['pneg'] += 1

        true_false_b.append(b==g)
        true_false_p.append(p==g)


    print(r)
    print("number of discordants = "+str(abs(r['bneg']['ppos'] - r['bpos']['pneg'])))
    chi2 = ((r['bpos']['pneg'] - r['bneg']['ppos'])**2) / (r['bpos']['pneg'] + r['bneg']['ppos'])

    print(chi2)

    chisquare(true_false_b, true_false_p, 1)
        



if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("gold")
    argparser.add_argument("baseline")
    argparser.add_argument("preds")
    args = argparser.parse_args()

    
    gold = read_list(args.gold)
    baseline = read_list(args.baseline)
    pred = read_list(args.preds)
      
    gold, baseline = normalise_classes(gold, baseline)
    gold, pred = normalise_classes(gold, pred)
    
    mcnemars(gold, baseline, pred)


