import argparse
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding
from keras.layers import LSTM
from keras.datasets import imdb
from sklearn.metrics import f1_score
from keras.utils.np_utils import to_categorical
from itertools import chain
from keras.preprocessing.text import one_hot as one_hot


class Vocab():
    def __init__(self):
        self.word2idx

def macrof(y_true, y_pred):
    return f1_score(y_true, y_pred)


def get_vocaulary(data):
    words = sorted(list(set(chain.from_iterable(data))))
    word2int = dict((word, i) for i, word in enumerate(words))
    return word2int


def read_data(data_filename):
    x, y = [], []
    if ".gz" in data_filename: fp = gzip.open(data_filename, "r", encoding="utf-8")
    else:  fp = open(data_filename, "r", encoding="utf-8")
    for line in fp:
        line = line.strip()
        x.append(line[1:])
        y.append(line[0])
    fp.close()
    return x, y 


def shuffle_data():
    data.shuffle()

def data2int(data, vocab):
    for s in range(0, len(sentences)):
        sentences[s][0] = one_hot(sentences[s][0], len(vocab), lower=True, split=" ")
    return sentences

def get_data(data_filename):
    data = read_data
    vocab = get_vocabulary
    data = one_hot_sentences(data, vocab)
    return data, vocab

if __name__=="__main__":
    argparser=argparse.ArgumentParser()
    argparser.add_argument(train_filepath)
    args = argparser.parse_args

    #---------------- parameters
    max_features = 20000
    maxlen = 80  # cut texts after this number of words (among top max_features most common words)
    batch_size = 32
    

    #---------------- load data
    print('Loading data...')
    (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
    print(len(x_train), 'train sequences')
    print(len(x_test), 'test sequences')

    # x_train, y_train()
    
    for x in x_train:
        print(x)
        input()
    
    #---------------- get labels
    # categorical_labels = categorical_labels = to_categorical(int_labels, num_classes=None)
    
    
    print('Pad sequences (samples x time)')
    x_train = sequence.pad_sequences(x_train, maxlen=maxlen)
    x_test = sequence.pad_sequences(x_test, maxlen=maxlen)
    print('x_train shape:', x_train.shape)
    print('x_test shape:', x_test.shape)
    
    
    #---------------- build model
    print('Building model...')
    model = Sequential()
    model.add(Embedding(max_features, 128)) # embedding layer
    model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(1, activation='sigmoid'))
    
    #---------------- compile model
    model.compile(loss='binary_crossentropy',
                optimizer='adam',
                metrics=['accuracy', macrof])
    
    #---------------- training
    print('Training...')
    model.fit(x_train, y_train,
            batch_size=batch_size,
            epochs=15,
            validation_data=(x_test, y_test))
    score, acc = model.evaluate(x_test, y_test,
                                batch_size=batch_size)
    print('Test score:', score)
    print('Test accuracy:', acc)
    
    
    

