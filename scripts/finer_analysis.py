import argparse
import gzip
import operator

def read_pred(filename):
    preds = []
    if ".gz" in filename: fp = gzip.open(filename, "rt")
    else: fp = open(filename, "r")
    for line in fp:
        line = line.strip()
        if line=="okay": line="ok"
        preds.append(line)
    fp.close()
    return preds
        

def get_different(gold, baseline, pred):

    bwd = []
    better = []
    worse = []
    different = []
    for b, p, g in zip(baseline, pred, gold):
        if b != p:
            if p==g:
                better.append((b, p))
                bwd.append(1)
            elif b==g:
                worse.append((b, p))
                bwd.append(0)
            else:
                different.append((b, p))
                bwd.append(-1)
        else:
            bwd.append(2)

    print("Total num of examples = "+str(len(gold)))
    print("Total num different = "+str(len(better) + len(worse) + len(different)))
    print("\nNum better in pred = "+str(len(better)))
    print("Num worse in pred = "+str(len(worse)))
    print("Num just different in pred = "+str(len(different)))

    # which classes?
    class2num_better = {}
    for tag in better:
        if tag not in class2num_better: class2num_better[tag] = 0
        class2num_better[tag] += 1
        
    class2num_worse = {}
    for tag in worse:
        if tag not in class2num_worse: class2num_worse[tag] = 0
        class2num_worse[tag] += 1
        
    sorted_better = sorted(class2num_better.items(),key=operator.itemgetter(1), reverse=True)
    print("Most frequent changes when better = ")
    for i in range(0,10):
        print("\t"+str(sorted_better[i]))

    sorted_worse = sorted(class2num_worse.items(),key=operator.itemgetter(1), reverse=True)
    print("Most frequent changes when worse = ")
    for i in range(0,10):
        print("\t"+str(sorted_worse[i]))

    return bwd
    

def analyse_beer(bwd, bbeer, pbeer, baseline, pred, gold):

    assert(len(bwd)==len(bbeer)==len(pbeer)==len(pred)==len(gold)==len(baseline))

    corr = {}
    for prec in ["better", "worse", "diff"]:
        for beer in ["higher", "lower", "same"]:
            corr[prec+beer] = []
    
    for j, (change, bb, pb, b, p)  in enumerate(zip(bwd, bbeer, pbeer, baseline, pred)):
        bb = bb.split()[-1]
        pb = pb.split()[-1]

        if change==2 and pb!=bb:
            
            raise Exception("The sentences are the same yet Beer gives different scores!! line "+str(j)+"\n")
        else:
            continue
        
        if change==1: keyword="better"
        elif change==0: keyword="worse"
        else: keyword="diff"
            
        if pb > bb: keyword += "higher"
        elif pb==bb: keyword += "same"
        else: keyword += "lower"

            
        corr[keyword].append((b,p))


    print("\nLooking at correlation with beer")
    # look at results
    for keyword in sorted(corr):
        print("Num of "+keyword+" = "+str(len(corr[keyword])))

        change2occ = {}
        for change in set(corr[keyword]):
            change2occ[change] = len([x for x in corr[keyword] if x==change])
        # print most frequent
        ordered = sorted(change2occ.items(),key=operator.itemgetter(1), reverse=True)
        for i in range(0,5):
            print("\t"+str(ordered[i]))
    
        
if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("gold")
    argparser.add_argument("baseline_pred")
    argparser.add_argument("predictions_pred")
    argparser.add_argument("baseline_beer")
    argparser.add_argument("predictions_beer")
    args = argparser.parse_args()

    gold = read_pred(args.gold)
    baseline = read_pred(args.baseline_pred)
    pred = read_pred(args.predictions_pred)
    beer_baseline = read_pred(args.baseline_beer)[:-1]
    beer_pred = read_pred(args.predictions_beer)[:-1]
    
    bwd = get_different(gold, baseline, pred)

    analyse_beer(bwd, beer_baseline, beer_pred, baseline, pred, gold)

    

