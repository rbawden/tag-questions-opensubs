from readfile import *
import os, argparse, gzip

def create_final(predfile, lines, total):

    lines = read_lines(lines)
    last = 0
    if ".gz" in predfile: fp = gzip.open(predfile)
    else: fp = open(predfile)

    for j, line in enumerate(fp.readlines()):
        if j!=0: last = lines[j-1]
        os.sys.stdout.write("none\n"*(lines[j]-last-1)+line)
        os.sys.stderr.write(str(j)+"\r")

        # input()
    fp.close()

    os.sys.stdout.write("none\n"*(total-lines[-1]))


if __name__=="__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("predfile")
    argparser.add_argument("lines")
    argparser.add_argument("total")

    args = argparser.parse_args()


    create_final(args.predfile, args.lines, int(args.total))
