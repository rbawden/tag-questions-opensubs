from evaluate_precision_gramtags import *
from sklearn.metrics import precision_recall_fscore_support
import numpy as np
import pandas as pd
import argparse, os, gzip

# if args.t=="tagnottag":
#     cat2num = {'-1':'False', '1':'True'}
# elif args.t=="fine":
#     cat2num = {'1':'none', '2':'pospos', '3':'posneg', '4':'negpos',
#                '5':'negneg', '6':'misctags'}
# elif args.t=="finer":
#     cat2num = {'1':'none', '2':'pospos', '3':'posneg', '4':'negpos',
#                '5':'negneg', '6':'miscposanchor', '7':'miscneganchor'}

def get_true_pred(gold, pred):
    y_true = []
    y_pred = []
    if ".gz" in gold: gf = gzip.open(gold, "rt")
    else: gf = open(gold, "r")
    if ".gz" in pred: pf = gzip.open(pred, "rt")
    else: pf = open(pred, "r")

    for g, p in zip(gf, pf):
        # print(g)
        # print(p)
        g, p = g.strip().lower(), p.strip().lower()
        if g=="" or p=="": break
        y_true.append(g)
        y_pred.append(p)
    if len(y_true)!=len(y_pred):
        exit("Gold and pred do not have an equal number of elements")
    classes = sorted(set(y_true).union(set(y_pred)))
    gf.close()
    pf.close()
    return y_true, y_pred, classes

def get_cross_tab(y_true, y_pred):
    print(pd.crosstab(pd.Series(y_true), pd.Series(y_pred), rownames=['True'], colnames=['Predicted'], margins=True))

def get_p_micro(y_true, y_pred, show=False):
    # prec, recall, fscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                            # np.array(y_pred),
                                                            # average='micro')    
    # if show:
    #     print("Micro:")
    #     print("\tP:"+str(prec))
    #     print("\tR:"+str(recall))
    #     print("\tF:"+str(fscore))
    #     print("---------------------------")

    correct = 0
    for i, y in enumerate(y_true):
        if y==y_pred[i]: correct += 1
    if len(y_true)==0: return 0
    return correct/float(len(y_true))
    
def get_f_each_label(y_true, y_pred, classes, show=False):
    prec, recall, fscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                    np.array(y_pred),
                                                    labels=classes,
                                                    average=None)
    if show:
        print("Prec, recall and fscore for each label")
        print(prec)
        print(recall)
        print(fscore)
        print("---------------------------")
    return fscore

def get_f_binary(y_true, y_pred, show=False):
    # print(y_true)
    # input()
    # print(y_pred)
    binprec, binrecall, binfscore, _ = precision_recall_fscore_support(np.array(y_true),
                                                                        np.array(y_pred),
                                                                        pos_label='true',
                                                                        average='binary')
    if show:    
        print("Binary (True):")
        print("\tP:"+str(binprec))
        print("\tR:"+str(binrecall))
        print("\tF:"+str(binfscore))
        print("---------------------------")
    return binfscore

def get_data_no_nones(y_true, y_pred):
    y_pred_no_nones = [y_pred[y] for y in range(len(y_pred)) if y_true[y] != "none"]
    y_true_no_nones = [y for y in y_true if y != "none"]

    return y_true_no_nones, y_pred_no_nones   

def get_p_r_f_nones(y_true, y_pred):
    correct = len([y for i, y in enumerate(y_true) if y_pred[i]=="none" and y=="none"])
    num_true = len([y for y in y_true if y=="none"])
    num_pred = len([y for y in y_pred if y=="none"])
    
    if num_pred==0: p=0
    else: p = correct/float(num_pred)
    if num_true==0: r=0
    else: r = correct/float(num_true)
    if p+r==0: f=0
    else: f = (2*p*r)/float(p+r)
    return p, r, f


def get_p_r_f_gramMiscNone(y_true, y_pred):
    p, r, f, _ = precision_recall_fscore_support(np.array(y_true),np.array(y_pred),
                                                 labels = ["gram", "misc", "none"],
                                                average=None)
    return p[0], r[0], f[0], p[1], r[1], f[1], p[2], r[2], f[2], 

def get_p_r_f_without_none(y_true, y_pred, classes, show=False):
                
    # y_true, y_pred = get_data_no_nones(y_true, y_pred)
    # prec, recall, fscore, _ = precision_recall_fscore_support(np.array(y_true),
    #                                                           np.array(y_pred),
    #                                                           average='micro')
    y_true1, y_pred1 = get_data_no_nones(y_true, y_pred)
    correct = 0
    for i, y in enumerate(y_true1):
        if y==y_pred1[i]: correct +=1
    if len(y_true1)==0: recall = 0
    else: recall = correct/float(len(y_true1))

    y_pred2, y_true2 = get_data_no_nones(y_pred, y_true)
    correct = 0
    for i, y in enumerate(y_true2):
        if y==y_pred2[i]: correct +=1
    if len(y_pred2)==0: precision = 0
    else: precision = correct/float(len(y_pred2))

    if precision+recall==0: fscore = 0
    else: fscore = (2 * precision * recall) / float(precision + recall)

    os.sys.stderr.write("num correct = "+str(correct)+"\n")
        
    # if show:
    #     print("Micro (only on real TQs):")
    #     print("\tP:"+str(prec))
    #     print("\tR:"+str(recall))
    #     print("\tF:"+str(fscore))

    # classes_zero = [c for c in classes if c not in y_true and c not in y_pred]
    
    # correct = len([y for i, y in enumerate(y_true) if y_pred[i]==y])
    # os.sys.stderr.write(str(correct/float(len(y_true)))+"\n")
    
    return precision, recall, fscore


def merge_classes(y_true, y_pred):
    for y_list in [y_true, y_pred]:
        for y in range(0,len(y_list)):
            if y_list[y]=="okay": y_list[y]="ok"
    return y_true, y_pred
        


if __name__=="__main__":

    argparser = argparse.ArgumentParser()
    argparser.add_argument("gold")
    argparser.add_argument("pred")
    argparser.add_argument("-binary", action='store_true', default=False)
    argparser.add_argument("-noshow", action='store_true', default=False)
    argparser.add_argument("-t", choices=["tagnottag", "fine", "finer", "real", "gramMiscNone", "gramMiscTypeNone"], required=True)
    args = argparser.parse_args()

    y_true, y_pred, classes = get_true_pred(args.gold, args.pred)
    y_true, y_pred = merge_classes(y_true, y_pred)

    numok_true = len([x for x in y_true if x=="ok"])
    numok_pred = len([x for x in y_pred if x=="ok"])
    os.sys.stderr.write(str(numok_true)+"\n")
    os.sys.stderr.write(str(numok_pred)+"\n")

    
    if args.binary:
        fscore = get_f_binary(y_true, y_pred, not args.noshow)
        os.sys.stdout.write(str(fscore))

    elif args.t=="gramMiscNone":
        allresults = get_p_r_f_gramMiscNone(y_true, y_pred)
        os.sys.stdout.write("\t".join([str(res) for res in allresults])+"\n")
        
    else:
        p_micro = get_p_micro(y_true, y_pred, not args.noshow)
        p_tq, r_tq, f_tq = get_p_r_f_without_none(y_true, y_pred, classes, not args.noshow)
        f_each = get_f_each_label(y_true, y_pred, classes, not args.noshow)
        pnone, rnone, fnone = get_p_r_f_nones(y_true, y_pred)

        os.sys.stdout.write(" ".join([str(x) for x in [p_tq, r_tq, f_tq, pnone, rnone, fnone, p_micro]])+"\n")
        

