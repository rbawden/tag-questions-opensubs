#!/usr/bin/python3
import csv, argparse

# read tab-separated file containing manual annotations of large coverage
# grammatical tag question candidates. Columns=line number, text, tag, not tag,
# mid-utterance tag, no anchor in subtitle
def read_annotated(filename):
    subs = []
    with open(filename, "r") as fp:
        reader = csv.reader(fp, delimiter="\t", quoting=csv.QUOTE_NONE)
        for i, line in enumerate(reader):
            if i==0: continue # ignore headers
            if "".join(line).strip()=="": break # end of file    
            line[0] = int(line[0])
            for i in range(2,len(line)):
                if line[i]=="x": line[i] = True
                else: line[i] = False
            subs.append(line)

    return subs

# read the file containing the list of lines automatically identified as tag
# questions
def read_lines(filename):
    lines = []
    with open(filename, "r") as fp:
        for line in fp:
            lines.append(int(line.strip()))
    return lines

# compare the lines present in the manually annotated file with what was found
# in the automatic evaluation
def evaluate(manual, auto):
    mid_not_found = []
    mid_found = []
    final_not_found = []
    final_found = []
    not_tag = []
    noanchor_tag = []
    mid_noanchor = []
    false_positive = []
    
    num, text, tag, nottag, mid, noanchor = 0,1,2,3,4,5 # shortcuts for indices of list
    
    for line in manual:
        # if line[noanchor]: continue # skip noanchor
            
        if line[tag] and line[mid]:
            if line[num] in auto: mid_found.append(line)
            else: mid_not_found.append(line)
        elif line[tag]:
            if line[num] in auto: final_found.append(line)
            else: final_not_found.append(line)
        elif line[nottag] and line[noanchor]: # could be a tag
            noanchor_tag.append(line)
            if line[mid]: mid_noanchor.append(line)
        else:
            if line[num] in auto: false_positive.append(line)
            not_tag.append(line)

    print("Potential tags with no anchors = "+str(len(noanchor_tag)), end="")
    print(" of which "+str(len(mid_noanchor))+" are mid-subtitle")
    print("Utterance-final tags = "+str(len(final_not_found)+len(final_found))+", ", end="")
    print("found = "+str(len(final_found))+", ", end="")
    print("not found = "+str(len(final_not_found)))
    print("Mid-utterance tags = "+str(len(mid_not_found)+len(mid_found))+", ", end="")
    print("found = "+str(len(mid_found))+", ", end="")
    print("not found = "+str(len(mid_not_found)))
    print("False positives (not tags but automatically classified as one): "+str(len(false_positive)))

    print("\n")
    print("Mid utterance tags found = ")
    for mid in mid_found:
        print(mid)

    print("\n")
    print("Utterance-final tags not found = ")
    for final in final_not_found:
        print(final)    

    print("\n")
    print("False positives found (not tags, but classed as such in automatic classification) = ")
    for false in false_positive:
        print(false)    
    print("\n")
     
    
argparser = argparse.ArgumentParser()
argparser.add_argument("annotated_file")
argparser.add_argument("automatically_identified_tags")
args = argparser.parse_args()

manual = read_annotated(args.annotated_file)
auto = read_lines(args.automatically_identified_tags)
evaluate(manual, auto)



