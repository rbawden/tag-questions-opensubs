#!/bin/sh

lambda=$1
lambdalex=$2
langpair=$3
scriptdir=$4
set=$5
vw=$6x
outputdir=$7
expdir=$8

# Train lex model

numlabels=`cat $outputdir/num_labels.lex.dev1.var.$langpair`
if [ numlabels != "0" ]; do
	numexamples=`wc -l $outputdir/lexexamples.dev1`
	echo ">> Training lex classifier on $numexamples"
	cat $outputdir/lexexamples.dev1 | perl -pe "s/\'/\-/g" \
		| eval "$(python3 $scriptdir/calculate_weighting.py $outputdir/classcounts.lex.dev1.var.$langpair $lambdalex)" \
		| perl -pe 's/\t/ |f /' | $vw -f $outputdir/$lambda.$lambdalex.dev1.lex.$langpair \
									  --named_labels `cat $outputdir/named_labels.lex.dev1.var.$langpair` \
									  --oaa `cat $outputdir/num_labels.lex.dev1.var.$langpair` -q ff --l2=1e-6 --ftrl

	
	# Predict on $set set
	perl -pe 's/^/ |f /' $outputdir/lexexamples.$set \
		| $vw -i $outputdir/$lambda.$lambdalex.dev1.lex.$langpair \
			  -t -p $outputdir/pred.$lambda.$lambdalex.$set.lex.$langpair 
	
	# Restick lexical predictions to main predictions
	python3 $scriptdir/restick_lex_preds.py $outputdir/pred.$lambda.$set.gramMiscNone.single.$langpair \
			$outputdir/pred.$lambda.$lambdalex.$set.lex.$langpair $outputdir/lexlines.$set \
			> $outputdir/pred.$lambda.$lambdalex.$set.restucklex.$langpair 
	
	# Predict grammatical rules
	python3 $scriptdir/rule-based2.py baseline/$set.all.$langpair.$(TRG).gz \
			gold/$set.gold.real.$langpair.gz \
			-c $outputdir/pred.$lambda.$lambdalex.$set.restucklex.$langpair \
			> $outputdir/pred.$lambda.$lambdalex.$set.real.final.$langpair 
	
	# Turn into gramMiscTypeNone for evaluation
	paste -d "&" $outputdir/pred.$lambda.$lambdalex.$set.restucklex.$langpair \
		  $outputdir/pred.$lambda.$lambdalex.$set.real.final.$langpair \
	| perl -pe 's/[^&\s]+&none/none&none/' \
	| perl -pe 's/([^&\s]+)&([^&\s]+)/\1/' \
		   > $outputdir/pred.$lambda.$lambdalex.$set.gramMiscTypeNone.$langpair
	
	# Evaluate
	res1=$( python3 $scriptdir/newevaluation.py$expdir/gold/$set.gramMiscTypeNone.$langpair.gz \
					$outputdir/pred.$lambda.$lambdalex.$set.gramMiscTypeNone.$langpair -t gramMiscTypeNone -noshow ) 
	
	res2=$( python3 $scriptdir/newevaluation.py$expdir/gold/$set.real.$langpair.gz \
					$outputdir/pred.$lambda.$lambdalex.$set.real.final.$langpair -t real -noshow ) ; \
	
	# Print out result
	echo "$lambda $lambdalex $res1 $res2"
	exit
done

echo "$lambda $lambdalex -NA-"

