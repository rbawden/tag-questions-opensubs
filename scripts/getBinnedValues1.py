import os
import argparse

def get_values(pred, threshold):

    if not os.path.exists(pred) and ".gz" not in pred: pred+=".gz" # try .gz file
    elif not os.path.exists(pred) and ".gz" in pred: pred = pred.replace(".gz", "")
    if ".gz" in pred: pp = gzip.open(pred, "rt")
    else:  pp = open(pred, "r")
    
    for i, p in enumerate(pp):
        p = p.strip()
            
        if float(p)> threshold:
            os.sys.stderr.write(str(i+1)+"\n")
            os.sys.stdout.write("firstclassifier:"+str(round(float(p), 2))+" ")
            if float(p) == 1 :
                os.sys.stdout.write("isOne ")
            elif float(p) > 0.5:
                os.sys.stdout.write("greaterThanHalf ")
            elif float(p) > 0:
                os.sys.stdout.write("greaterThan0 ")
            elif float(p) > -0.5:
                os.sys.stdout.write("greaterThanMinusHalf ")
            elif float(p) < -0.8:
                os.sys.stdout.write("LessThanMinusZeroEight ")
            elif float(p) < -0.9:
                os.sys.stdout.write("LessThanMinusZeroNine ")
            elif float(p) < -0.95:
                os.sys.stdout.write("LessThanMinusZeroNineFive ")
            elif float(p) < -0.97:
                os.sys.stdout.write("LessThanMinusZeroNineSeven ")
            elif float(p) < -0.98:
                os.sys.stdout.write("LessThanMinusZeroNineEight ")
            else:
                os.sys.stdout.write("VeryLowValue ")
            os.sys.stdout.write("\n")
    pp.close()

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser = argparse.ArgumentParser()
    parser.add_argument("threshold", type=float)
    parser.add_argument("predfile")
    args = parser.parse_args()

    get_values(args.predfile, args.threshold)

