from simple_rnn import *

def create_model(v, vec_size, window):
    src_toks = Input(shape=(window, ), name='src_toks', dtype='int32')
    tgt_toks = Input(shape=(window, ), name='tgt_toks', dtype='int32')
    src_embed = Embedding(len(v.src_toks), vec_size, input_length = window, mask_zero = True, name = 'src_embed')
    tgt_embed = Embedding(len(v.tgt_toks), vec_size, input_length = window, mask_zero = True, name = 'tgt_embed')
    src_vec = src_embed(src_toks)
    tgt_vec = tgt_embed(tgt_toks)
    lstm_src_bi = Bidirectional(LSTM(128, dropout=0.2, recurrent_dropout=0.2, return_sequences=False, name='src_bilstm'))
    lstm_tgt_bi = Bidirectional(LSTM(128, dropout=0.2, recurrent_dropout=0.2, return_sequences=False, name='tgt_fwd'))
    lstm_src_bi_out = lstm_src_bi(src_vec)
    lstm_tgt_bi_out = lstm_tgt_bi(tgt_vec)
    merged_layer = concatenate([lstm_src_bi_out, lstm_tgt_bi_out], axis=-1)
    dense_output = Dense(256, activation='sigmoid')(merged_layer)
    predictions = Dense(len(v.labels), activation='softmax', name='predictions')(dense_output)
    model = Model(inputs=[src_toks, tgt_toks], outputs = predictions)
    return model


if __name__=="__main__":
    
    argparser = argparse.ArgumentParser()
    argparser.add_argument("data", help="training data file")
    argparser.add_argument("vocab", help="dev data file")
    argparser.add_argument("model", help="dev data file")
    argparser.add_argument("outfile", help="output folder path")
    args = argparser.parse_args()

    window = 90
    vec_size = 90

    with open(args.vocab, "rb") as fp:
        v = pickle.load(fp)

    v.training = False
    x_src, x_tgt, y, _ = get_matrix(v, args.data, window)
    x_src = sequence.pad_sequences(x_src, maxlen = window)
    x_tgt = sequence.pad_sequences(x_tgt, maxlen = window)

    model = create_model(v, window, vec_size)
    model.load_weights(args.model)

    predictions = model.predict([x_src, x_tgt])

    labels = get_argmax_labels(predictions, v)
    for label in labels:
        print(label)

    
