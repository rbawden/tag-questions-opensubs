import argparse, gzip, os, pickle
import collections
from keras.preprocessing import sequence
from keras.models import Sequential, Model
from keras.layers import Dense, Embedding, Input, concatenate, Dropout, Bidirectional
from keras.layers import LSTM

from keras.datasets import imdb
import numpy as np
from keras.utils import plot_model, np_utils
from keras.callbacks import ModelCheckpoint


#-------------------------------------------------------------------------------
# Read vocabulary from training data
#-------------------------------------------------------------------------------
class Vocab(object):
    def __init__(self):
        self.src_toks = {u"<MASK>":0, u"<UNK>":1}
        self.tgt_toks = {u"<MASK>":0, u"<UNK>":1}
        self.labels = {u"<MASK>":0, u"<UNK>":1}

        self.label_counter = collections.Counter()
        self.src_counter = collections.Counter()
        self.tgt_counter = collections.Counter()

        self.training = True

    def get_id(self, label, dict, counter=None):
        if self.training:
            if counter is not None: counter.update([label])
            return dict.setdefault(label, len(dict))
        else:
            if dict == self.labels:
                return dict.get(label, dict["<UNK>"])
                
            if counter is not None:
                if counter[label] == 1: return dict.get(label, dict[u"<UNK>"])
            return dict.get(label, dict[u"<UNK>"])


def get_argmax_labels(predictions, v):
    labels = []
    num2labels = {v: k for k, v in v.labels.items()}
    for p, predict in enumerate(predictions):
        maxlabel = list(predict).index(max(predict))
        labels.append(num2labels[maxlabel])
    return labels
        
def read_vocab(data_fname, rebuild=False):
    os.sys.stderr.write("Reading vocab\n")
    if ".gz" in data_fname: data_fp = gzip.open(data_fname, "rt", encoding="utf-8")
    else: data_fp = open(data_fname, "r", encoding="utf-8")
    save_location = data_fname+".vobab.pickle"

    # if exists already and not redoing vocab
    if not rebuild and os.path.exists(save_location):
        os.sys.stderr.write("Loading vocab from "+str(save_location)+"\n")
        return load_vocab(save_location)

    # otherwise read vocab
    v = Vocab()
    i=0
    for row in data_fp:
        i+=1
        if i%1000==0: os.sys.stderr.write(str(i)+"\r")
        if row.strip()=="": continue # ignore blank lines
        
        if len(row.strip().split("\t"))==2:
            label, src = row.strip().split("\t")
            tgt = ""
        else:
            label, src, tgt = row.strip().split("\t")
        src_toks = src.split(" ")
        tgt_toks = tgt.split(" ")

        # get vocab
        for s_tok in src_toks: v.get_id(s_tok, v.src_toks, v.src_counter)
        for t_tok in tgt_toks: v.get_id(t_tok, v.tgt_toks, v.tgt_counter)
        v.get_id(label, v.labels, v.label_counter)

    os.sys.stderr.write("Saving vocab to "+str(save_location)+"\n")
    save_vocab(v, save_location)
    return v

def save_vocab(v, f_name):
    with open(f_name, "wb") as fp:
        pickle.dump(v, fp, pickle.HIGHEST_PROTOCOL)

def load_vocab(f_name):
    with open(f_name, "rb") as fp:
        return pickle.load(fp)


def get_example_count(train_fname, v, window):
    count = 0
    ms = make_matrices(1, window, len(v.label))
    raw_data = infinite_iter_data(training_fname, max_rounds=1)
    for minibatch in fill_batch(ms, v, raw_data):
        count += 1
    return count


def get_matrix(v, data_fname, window):
    os.sys.stderr.write("Getting matrix of data\n")
    if ".gz" in data_fname: data_fp = gzip.open(data_fname, "rt", encoding="utf-8")
    else: data_fp = open(data_fname, "r", encoding="utf-8")

    label2num = {}
    X_src, X_tgt, Y = [], [], []
    for row in data_fp:
        if len(row.strip().split("\t"))==2:
            label, src= row.strip().split("\t")
            tgt = ""
        else:
            label, src, tgt = row.strip().split("\t")
        src_vec = [v.get_id(s_tok, v.src_toks) for s_tok in src.split()]
        tgt_vec = [v.get_id(t_tok, v.tgt_toks) for t_tok in tgt.split()]
        X_src.append(src_vec)
        X_tgt.append(tgt_vec)
        Y.append(v.get_id(label, v.labels))
        if v.get_id(label, v.labels) not in label2num: label2num[v.get_id(label, v.labels)] = 0
        label2num[v.get_id(label, v.labels)] += 1
            

    return X_src, X_tgt, Y, label2num


def get_class_weights(label2num, mu=0.15):
    classweights = {}
    # print(label2num)
    # labels = np_utils.to_categorical(list(label2num.keys()), max(label2num)+1)
    # print(labels)
    maxval = max(label2num.values())
    total = sum(label2num.values())
    for l, label in enumerate(label2num):
        classweights[label] = (mu*maxvalue+(1-mu)*label2num[label])/float(label2num[label])
    return classweights

if __name__=="__main__":
    
    argparser = argparse.ArgumentParser()
    argparser.add_argument("train", help="training data file")
    argparser.add_argument("dev", help="dev data file")
    argparser.add_argument("out", help="output folder path")
    argparser.add_argument("-f", "--force_rebuild", action="store_true", default=False)
    args = argparser.parse_args()
    
    vocab=set()
    vocab2index = None
    index2vocab = None
    dist_labels= None
    label2index = None
    index2label = None
    window = 30
    vec_size = 90
    minibatch_size = 1000
    classweights_lambda = 0.01

    
    max_features = 20000

    #---------------------------------------------------------------------------
    # command line arguments
    #---------------------------------------------------------------------------
    train_fname = args.train
    dev_fname = args.dev

    #---------------------------------------------------------------------------
    # load data
    #---------------------------------------------------------------------------
    v = read_vocab(train_fname, rebuild = args.force_rebuild)

    x_train_src, x_train_tgt, y_train, label2num = get_matrix(v, train_fname, window)
    v.training = False
    x_test_src, x_test_tgt, y_test, _ = get_matrix(v, dev_fname, window)
    classweights = get_class_weights(label2num, classweights_lambda)

    
    # (x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=max_features)
    print(len(x_train_src), 'train sequences')
    print(len(x_test_src), 'test sequences')

    # labels
    y_train = np_utils.to_categorical(y_train, len(v.labels))
    y_test = np_utils.to_categorical(y_test, len(v.labels))
    print('y_train shape:', y_train.shape)
    print('y_test shape:', y_test.shape)

    
    # pad sequences
    x_train_src = sequence.pad_sequences(x_train_src, maxlen = window)
    x_train_tgt = sequence.pad_sequences(x_train_tgt, maxlen = window)
    x_test_src = sequence.pad_sequences(x_test_src, maxlen = window)
    x_test_tgt = sequence.pad_sequences(x_test_tgt, maxlen = window)
    print('x_train shape:', x_train_src.shape)
    print('x_test shape:', x_test_src.shape)
    

    # build model
    print('Build model...')

    # inputs
    src_toks = Input(shape=(window, ), name='src_toks', dtype='int32')
    tgt_toks = Input(shape=(window, ), name='tgt_toks', dtype='int32')
    
    # embeddings
    src_embed = Embedding(len(v.src_toks), vec_size, input_length = window, mask_zero = True, name = 'src_embed')
    tgt_embed = Embedding(len(v.tgt_toks), vec_size, input_length = window, mask_zero = True, name = 'tgt_embed')
    src_vec = src_embed(src_toks)
    tgt_vec = tgt_embed(tgt_toks)
    
    # lstm layer 1
    lstm_src_bi = Bidirectional(LSTM(128, dropout=0.2, recurrent_dropout=0.2, return_sequences=False, name='src_bilstm'))
    lstm_tgt_bi = Bidirectional(LSTM(128, dropout=0.2, recurrent_dropout=0.2, return_sequences=False, name='tgt_fwd'))

    lstm_src_bi_out = lstm_src_bi(src_vec)
    lstm_tgt_bi_out = lstm_tgt_bi(tgt_vec)

    # merged layer
    merged_layer = concatenate([lstm_src_bi_out, lstm_tgt_bi_out], axis=-1)

    # final layer
    dense_output = Dense(256, activation='sigmoid')(merged_layer)
    predictions = Dense(len(v.labels), activation='softmax', name='predictions')(dense_output)

    model = Model(inputs=[src_toks, tgt_toks], outputs = predictions)
    
    # compile
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    # plot_model(model, to_file='model.png')

    check = ModelCheckpoint(filepath=args.out+"/src_trg.clweights="+str(classweights_lambda)+"hdf5",
                    verbose=1, save_best_only=True)
    
    # training 
    print('Train...')
    model.fit(x = [x_train_src, x_train_tgt], y = y_train,
              batch_size = minibatch_size,
              epochs=5,
              validation_data=([x_test_src, x_test_tgt], y_test),
              callbacks = [check],
              class_weight = classweights)
    
    score, acc = model.evaluate([x_test_src, x_test_tgt], y_test,
                                batch_size = minibatch_size)
    print('Test score:', score)
    print('Test accuracy:', acc)
    
    
