#!/usr/bin/python3
import re
import codecs
import os
# coding: utf-8

def filter_sents(src_in, src_out):
    i = 0
    for src in src_in:
        i+=1
        # empty
        if src.strip()=="":
            os.sys.stderr.write(str(i)+"\n")
            continue
        # lines too long
        elif len(src.split()) > 300:
            os.sys.stderr.write(str(i)+"\n")
            continue
        else:
            src_out.write(src.strip()+"\n")


if __name__=="__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Create a parallel corpus from all folders')
    parser.add_argument("src_in")
    parser.add_argument("src_out")
    args = parser.parse_args()

    src_in = open(args.src_in, "r",encoding='utf-8')
    src_out = open(args.src_out, "w",encoding='utf-8')
    filter_sents(src_in, src_out)
    src_in.close()
    src_out.close()  
    
    
