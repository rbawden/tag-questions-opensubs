import argparse, os, re


def get_en_tags():
    return re.compile(".*(("+"|".join(get_en_aux())+") (?:n.t )?"+"("+"|".join(get_en_pros())+")(?: not)?)( [\,\.\;\:\\\/\'\"\`\-\?\!]|and|or|but|yet|while|whilst|after|although|as|how|if|when|where|so|since|though|that|before|even|just|provid|until|because|[\`\']?co[sz]|when|\'?til||unless|every|in|$)", re.I)

def get_en_pros():
    return ["I", "you", "[h\']e", "thee", "ye", "yer", "she+", "we", "one", "they", "it", "i\'", "there", "someone", "anyone"]

def get_en_aux():
    return ["have", "had", "has", "might", "may", "will", "would", "could",
              "can", "must", "should", "shall", "ought", "do",
              "am", "are", "is", "was", "were", "had", "ai", "dare", "need"]

def get_regex_en_special_tags():
    return re.compile("^(.+) ((?:inn+i.|\, ri+ght \?|e+h+ \?|\, see+ \?|\, remember \?|\, you know \?|\, or what \?|\, ye+a+h+ \?|\, aye|\, you see|\, like|\, ok(ay)? \?|do n't y..? think \?|\, correct \?)[ \"\'\?\.]*)$", re.I)

def get_en_neg_words():
    return ["n[\'o]t", "never", "no longer", "nobody", "nowhere", "nothin[^ ]?", "no-?one",
            "none", "[h\']ardly", "rarely", "scarcely", "seldom", "barely", "neither",
            "nor", "ne\'er", "[^ ]*nae", "nuffin[^ ]?", "can ?nae", "nowt"] # removed 'no' - treat separately

def get_tags(en_file):

    en_tags = get_en_tags()
    # en_special_tags = get_regex_en_special_tags()
    
    for i, line in enumerate(en_file):
        line = line.strip()
        found = False
        
        # all standard ones
        if re.match(en_tags, line):
            found = True

        # misc tags
        #elif re.match(misc, line):
        #    found = True

        if found:
            os.sys.stdout.write(str(i+1)+"\t"+line.strip()+"\n")
    


if __name__=="__main__":

    argparser = argparse.ArgumentParser()
    argparser.add_argument('en_file')
    args = argparser.parse_args()

    
    with open(args.en_file) as fp:
        get_tags(fp)
