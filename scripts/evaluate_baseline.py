from classify_tag_questions import *
from evaluate_precision_gramtags import *
import marshal
from sklearn.metrics import precision_recall_fscore_support
import pandas as pd
from get_gold import binary_search


def readtextfile(filename, fol, fileout):

    file_suffix = "dev1.list" #"raw.precleaned.birecoded.cleaned.noblank.melttok.truecased.2000-2016.en"
    gramtags = read_lines(fol+"/en_tag_questions_gram."+file_suffix)
    misctags = read_lines(fol+"/en_tag_questions_misc."+file_suffix)
    miscposanchor = read_lines(fol+"/en_tag_questions_misc_posanchor."+file_suffix)
    miscneganchor = read_lines(fol+"/en_tag_questions_misc_neganchor."+file_suffix)
    postag = read_lines(fol+"/en_tag_questions_gram_postag."+file_suffix)
    negtag = read_lines(fol+"/en_tag_questions_gram_negtag."+file_suffix)
    posanchor = read_lines(fol+"/en_tag_questions_gram_posanchor."+file_suffix)
    neganchor = read_lines(fol+"/en_tag_questions_gram_neganchor."+file_suffix)
    pospos = read_lines(fol+"/en_tag_questions_gram_postag_posanchor."+file_suffix)
    negneg = read_lines(fol+"/en_tag_questions_gram_negtag_neganchor."+file_suffix)
    negpos = read_lines(fol+"/en_tag_questions_gram_postag_neganchor."+file_suffix)
    posneg = read_lines(fol+"/en_tag_questions_gram_negtag_posanchor."+file_suffix)
    
    tag = [] # boolean list
    gold = []
    pred = []
    with open(filename, "r") as fp:
        for i, line in enumerate(fp):
            if i%1000==0: os.sys.stderr.write("\r"+str(i))
            
            tag.append(["none","none"]) # pred, truth
            # print(len(tag))
            line = line.strip()
            if is_en_tag(line): 
            
                # check anchor
                anchor, qtag = get_anchor_and_tag(line)
                if not anchor: continue 
                if not is_viable_anchor(anchor, qtag): continue

                # which tag?
                if is_en_misc_tag(line):
                    auxverb, tagpro = get_auxverb_tagpro(line)
                    if is_positive_anchor(anchor, auxverb, tagpro):
                        pred.append("misc_pos")
                        #tag[-1][0] = "misc_pos"
                    else:
                        pred.append("misc_neg")
                        #tag[-1][0] = "misc_neg"

                elif is_en_gram_tag_negtag(line):
                    auxverb, tagpro = get_auxverb_tagpro(line)
                    if is_positive_anchor(anchor, auxverb, tagpro):
                        pred.append("gram_posneg")
                        # tag[-1][0] ="gram_posneg"
                    else:
                        pred.append("gram_negpos")
                        # tag[-1][0] = "gram_negpos"

                elif is_en_gram_tag_postag(line):
                    auxverb, tagpro = get_auxverb_tagpro(line)
                    if is_positive_anchor(anchor, auxverb, tagpro):
                        pred.append("gram_pospos")
                        # tag[-1][0] = "gram_pospos"
                    else:
                        pred.append("gram_negpos")
                        # tag[-1][0] = "gram_negpos"
            else:
                pred.append("none")
                    
                
            # os.sys.stdout.write(tag[-1][0]+"\n")        
                
            # gold tag question
            if binary_search(i, gramtags):
                # print(len(tag))
                # tag[-1][1] = "gram"
                if binary_search(i, pospos):
                    gold.append("gram_pospos")
                    # tag[-1][1] += "_pospos"
                elif binary_search(i, posneg):
                    gold.append("gram_posneg")
                    # tag[-1][1] += "_posneg"
                elif binary_search(i, negpos):
                    gold.append("gram_negpos")
                    # tag[-1][1] += "_negpos"
                elif binary_search(i, negneg):
                    gold.append("gram_negneg")
                    # tag[-1][1] += "_negneg"
            elif binary_search(i, misctags):
                # tag[-1][1] = "misc"
                if binary_search(i, miscposanchor):
                    gold.append("misc_pos")
                    # tag[-1][1] += "_pos"
                elif binary_search(i, miscneganchor):
                    gold.append("misc_neg")
                    # tag[-1][1] += "_neg"
            else:
                gold.append("none")
            # print(tag)
            # input()

            if len(gold)!=len(pred):
                print(gold)
                print(pred)
                exit("Unequal lengths")
    marshal.dump([gold, pred], open(fileout, 'wb'))
    return gold, pred

def evaluate(gold, pred, marshalfile=None, binary=True):

    if marshalfile:
        os.sys.stderr.write("loading from marshal file\n")
        [gold, pred] = marshal.load(open(marshalfile, "rb")) # hack



    if binary:
        binarygold = ["not-tag" if g=="none" else "tag" for g in gold]
        binarypred = ["not-tag" if p=="none" else "tag" for p in pred]

        prec, recall, fscore, _ = precision_recall_fscore_support(np.array(binarygold),
                                                                np.array(binarypred),
                                                                labels=["not-tag", "tag"],
                                                                average=None)
    
        cm = confusion_matrix(binarygold, binarypred)
        print(cm)
        print("--------")
        print(pd.crosstab(pd.Series(binarygold), pd.Series(binarypred), rownames=['True'], colnames=['Predicted'], margins=True))
    
        print(prec)
        print(recall)
        print(fscore)

        
    else:
        tag=[]
        
        os.sys.stderr.write("creating lists for evaluation\n")
        e_tag_pred = ["not-tag" if "none" == p else "tag" for p,_ in tag]
        e_tag_gold = ["not-tag" if "none" == g else "tag" for _,g in tag]
        e_grammisc_pred = ["gram" if "gram" in p else "misc" if "misc" in p else "none" for p,_ in tag]
        e_grammisc_gold = ["gram" if "gram" in g else "misc" if "misc" in g else "none" for _,g in tag]
        e_fine_pred = [p if "gram" in p or "misc" in p else "none" for p,_ in tag]
        e_fine_gold = [g if "gram" in g or "misc" in g else "none" for _,g in tag]
    
        os.sys.stderr.write("creating confusion matrices\n")
        # print(set(e_tag_pred))
        # print(set(tag))
    
        print("---------------------------")
        prec, recall, fscore, _ = precision_recall_fscore_support(np.array(e_tag_gold),
                                                                np.array(e_tag_pred),
                                                                average='micro')
        
        print("Tag/nottag (micro):")
        print("\tP:"+str(prec))
        print("\tR:"+str(recall))
        print("\tF:"+str(fscore))
    
        print("---------------------------")
    

        return 
        
        classes1 = sorted(['not-tag', 'tag'])
        cm1 = confusion_matrix(e_tag_gold, e_tag_pred)
        tn1, fp1, fn1, tp1 = cm1[0][0], cm1[0][1], cm1[1][0], cm1[1][1]
        # print((tn1, fp1, fn1, tp1))
        print(pd.crosstab(pd.Series(e_tag_gold), pd.Series(e_tag_pred), rownames=['True'], colnames=['Predicted'], margins=True))
        plt.figure(1)
        plt.subplot(221)
        plot_confusion_matrix(cm1, classes1)
    
        print("\nPer label")
        print("\tTag:     P = "+str(tp1/(tp1+fp1))+", R = "+str(tp1/(tp1+fn1)))
        print("\tNot Tag: P = "+str(tn1/(tn1+fn1))+", R ="+str(tn1/(tn1+fp1)))
        print(cm1)
    
        prec, recall, fscore, _ = precision_recall_fscore_support(np.array(e_tag_gold),
                                                                np.array(e_tag_pred),
                                                                labels=classes1,
                                                                average=None)
    
        print(prec)
        print(recall)
        print(fscore)
    
        return
    print("---------------------------")
    
    classes2 = sorted(['gram', 'misc', 'none'])
    cm2 = confusion_matrix(e_grammisc_gold, e_grammisc_pred)
    correct2 = 0
    for c in range(len(classes2)):
        correct2 += cm2[len(classes2)-c-1][c] 
        # plt.figure()
        plt.subplot(222)
        plot_confusion_matrix(cm2, classes2)
    
        print("---------------------------")
        
        classes3 = sorted(['gram_pospos', 'gram_negpos', 'gram_negneg', 'gram_posneg', 'none', 'misc_pos', 'misc_neg'])
        cm3 = confusion_matrix(e_fine_gold, e_fine_pred)
        correct3 = 0
        for c in range(len(classes3)):
            correct3 += cm3[len(classes3)-c-1][c] 
    
        # plt.subplot(223)
        plt.figure()
        plot_confusion_matrix(cm3, classes3)
    
        print("---------------------------")
    
        print("\nOverall evaluation:")
        print("\tIs it a tag?: P = "+str(tp1/float(tp1+fp1))+", R = "+str(tp1/float(tp1+fn1))+"\n")
        print("\tGram/misc?: P = "+str(correct2)+" out of "+str(len(e_tag_pred))+" ("+str(correct2/float(len(e_tag_pred)))+")")
        # print("\tCorrect type? (gram and misc together): P = "+str(correct3)+" out of "+str(len(e_tag_pred))+" ("+str(correct3/float(len(e_tag_pred)))+")")
        # plt.show()
    
def read_preds(filename, fol, fileout):
    file_suffix = "dev2.list" #"raw.precleaned.birecoded.cleaned.noblank.melttok.truecased.2000-2016.en"
    gramtags = read_lines(fol+"/en_tag_questions_gram."+file_suffix)
    misctags = read_lines(fol+"/en_tag_questions_misc."+file_suffix)
    miscposanchor = read_lines(fol+"/en_tag_questions_misc_posanchor."+file_suffix)
    miscneganchor = read_lines(fol+"/en_tag_questions_misc_neganchor."+file_suffix)
    postag = read_lines(fol+"/en_tag_questions_gram_postag."+file_suffix)
    negtag = read_lines(fol+"/en_tag_questions_gram_negtag."+file_suffix)
    posanchor = read_lines(fol+"/en_tag_questions_gram_posanchor."+file_suffix)
    neganchor = read_lines(fol+"/en_tag_questions_gram_neganchor."+file_suffix)
    pospos = read_lines(fol+"/en_tag_questions_gram_postag_posanchor."+file_suffix)
    negneg = read_lines(fol+"/en_tag_questions_gram_negtag_neganchor."+file_suffix)
    negpos = read_lines(fol+"/en_tag_questions_gram_postag_neganchor."+file_suffix)
    posneg = read_lines(fol+"/en_tag_questions_gram_negtag_posanchor."+file_suffix)
    
    tag = []
    with open(filename, "r") as fp:
        for i, line in enumerate(fp):
            if i%1000==0: os.sys.stderr.write("\r"+str(i))
            tag.append(["none", "none"])
            tag[-1][0] = line.strip()

            # gold tag question
            if i in gramtags:
                # print(len(tag))
                tag[-1][1] = "gram"
                if i in pospos: tag[-1][1] += "_pospos"
                elif i in posneg: tag[-1][1] += "_posneg"
                elif i in negpos: tag[-1][1] += "_negpos"
                elif i in negneg: tag[-1][1] += "_negneg"
            elif i in misctags:
                tag[-1][1] = "misc"
                if i in miscposanchor: tag[-1][1] += "_posanchor"
                elif i in miscneganchor: tag[-1][1] += "_neganchor"

    marshal.dump(tag, open(fileout, "wb"))
    return tag
                  
    
if __name__=="__main__":

    argparser = argparse.ArgumentParser()
    group = argparser.add_mutually_exclusive_group(required=True)
    group.add_argument("-m", nargs=1, metavar="marshalfile", help="load from marshal file")
    group.add_argument("-f", nargs=3, metavar=["translation", "folder_lines", "marshaloutput"],
                       help="regenerate from file") # 2 args: translated file, folder containing lines + marshal output
    
    
    
    # argparser.add_argument("whole_translated_file") # E.g. dev2
    # argparser.add_argument("tagline_file")
    # argparser.add_argument("folder_containing_line_files")
    # argparser.add_argument("marshalfile")
    # argparser.add_argument("-m", nargs=1, help="marshaldump", required=True)
    # argparser.add_argument('-f', nargs=1, help="fromtransfile")
    args = argparser.parse_args()

    # fol = args.folder_containing_line_files
    # if fol[-1]=="/": fol = fol[:-1]
    
    # tag = readtextfile(args.whole_translated_file, fol, args.marshalfile)
    # tag = read_preds(args.whole_translated_file, fol, args.marshalfile) # when a list of preds
    # evaluate(tag) # when generating directly

    if args.m:
        evaluate(None, None, args.m[0])
    elif args.f:
        gold, pred = readtextfile(args.f[0], args.f[1], args.f[2])
        evaluate(gold, pred)



